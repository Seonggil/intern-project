package com.tmoncorp.admin.service.search;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by KangSeongGil on 2017. 8. 3..
 */
@Component
public class SearchServiceFactory {

    @Autowired
    TmonSearchService tmonSearchService;
    @Autowired
    CoupangSearchService coupangSearchService;
    @Autowired
    WemakeSearchService wemakeSearchService;

    public SearchService getProduct(String corpName){
        if("tmon".equals(corpName) || "티몬".equals(corpName)) {
            return tmonSearchService;
        }else if("coupang".equals(corpName) || "쿠팡".equals(corpName)) {
            return coupangSearchService;
        }else if("we-make-price".equals(corpName) || "위메프".equals(corpName)) {
            return wemakeSearchService;
        }else {
            return null;
        }
    }
}
