package com.tmoncorp.admin.service.search;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmoncorp.admin.enums.CoupangDeliveryType;
import com.tmoncorp.admin.model.CoupangDetailInfo;
import com.tmoncorp.admin.model.CoupangItem;
import com.tmoncorp.admin.model.DealInfo;
import com.tmoncorp.admin.util.CrawlingUtil;
import com.tmoncorp.admin.util.StringUtil;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Created by KangSeongGil on 2017. 8. 1..
 */
@Service
public class CoupangSearchService implements SearchService {
    private Logger logger;

    public CoupangSearchService() {
        logger = Logger.getLogger(CoupangSearchService.class.getName());
    }

    @Override
    public List<CoupangItem> getSummaryList(String searchKeyword, String sortType) {
        List<CoupangItem> itemList = new ArrayList();
        CrawlingUtil crawlingUtil = new CrawlingUtil();
        logger.debug("--------------");
        logger.debug("coupang summary");

        Document doc = crawlingUtil.getSearchResultDocument(searchKeyword, sortType, "COUPANG");
        Element dealList = doc.select("ul[id=productList]").get(0);

        if (Objects.isNull(dealList)) {
            return null;
        }

        Iterator<Element> dealInfo = dealList.getElementsByTag("li").iterator();

        while (dealInfo.hasNext()) {
            Element dealItem = dealInfo.next();

            long dealNum = Long.parseLong(dealItem.attr("id"));
            Element detailDealInfo = dealItem.select("a").get(0);
            String dealUrl = "http://www.coupang.com" + detailDealInfo.attr("href");

            detailDealInfo = detailDealInfo.select("dl").get(0);
            String dealImgUrl = detailDealInfo.select("dt[class=image]").select("img").attr("src");
            if(dealImgUrl.equals("")) {
                dealImgUrl =  detailDealInfo.select("dt[class=image]").select("img").attr("data-img-src");
            }

            detailDealInfo = detailDealInfo.select("dd[class=descriptions]").get(0);
            String dealName = detailDealInfo.select("div[class=name]").text();

            Elements dealPriceInfo = detailDealInfo.select("div[class=price-wrap]");
            int priceKindAmount = dealPriceInfo.size();

            int rocketPrice = 0, itvPrice = 0, regularPrice = 0;
            if (priceKindAmount == 2) {
                Elements priceElement = dealPriceInfo.select("strong[class=price-value]");
                rocketPrice = StringUtil.removeComma(priceElement.get(0).text());
                regularPrice = rocketPrice;
            } else if (priceKindAmount == 1) {

                String priceString = dealPriceInfo.select("strong[class=price-value]").text();
                if (priceString.length() > 0) {
                    int price = StringUtil.removeComma(priceString);
                    if (dealPriceInfo.select("span[class=badge rocket]").size() == 1) {
                        rocketPrice = price;
                        regularPrice = price;
                    } else {
                        regularPrice = price;
                    }
                }
            }

            logger.debug("search finish");
            logger.debug("----------------");
            CoupangItem item = new CoupangItem(dealNum, dealName, dealImgUrl, regularPrice, dealUrl, rocketPrice, itvPrice);
            itemList.add(item);
        }
        return itemList;
    }

    @Override
    public void setDetailInfo(DealInfo dealInfo) throws Exception {
        logger.debug("--------------");
        logger.debug("coupang detailInfo");
        logger.debug("dealNum:"+dealInfo.getDealSrl());
        logger.debug("--------------");
        CoupangDetailInfo detailInfo = new CoupangDetailInfo();
        ObjectMapper objectMapper = new ObjectMapper();
        RestTemplate restTemplate = new RestTemplate();
        JsonNode rootNode = null;
        CrawlingUtil crawlingUtil = new CrawlingUtil();
        String crawlingResult = crawlingUtil.getDynamicDocument(dealInfo.getUrl());
        Document doc = Jsoup.parse(crawlingResult);
        Document deliveryDoc;

        Element topInfoElement = doc.select("section[id=contents]").get(0);
        long venderItem = Long.parseLong(topInfoElement.attr("data-vendor-item-id"));
        long dealNum = Long.parseLong(topInfoElement.attr("data-product-id"));

        String reviewUrl = "http://www.coupang.com/vp/products/" + dealNum + "/review-rating";
        String jsonData = restTemplate.getForObject(reviewUrl, String.class);


        try {
            rootNode = objectMapper.readTree(jsonData);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!Objects.isNull(rootNode)) {
            detailInfo.setReviewAmount(rootNode.path("ratingCount").asLong());
            detailInfo.setItemReviewScore(rootNode.path("ratingAverage").asDouble());
        }


        String deliveryUrl = "http://www.coupang.com/vp/products/" + Long.toString(dealNum) + "/vendor-items/"
                + Long.toString(venderItem) + "/sale-infos/sdp";
        String deliveryHTML = restTemplate.getForObject(deliveryUrl, String.class);
        deliveryDoc = Jsoup.parse(deliveryHTML);


        String dftPriceStr = deliveryDoc.select("div[class=prod-value-holder]").attr("data-sale-price");
        dealInfo.setPrice(Integer.parseInt(dftPriceStr));

        dftPriceStr = deliveryDoc.select("div[class=prod-value-holder]").attr("data-shipping-fee");
        int dftPrice = (int) (Double.parseDouble(dftPriceStr));
        detailInfo.setDeliveryPrice(dftPrice);

        if (deliveryDoc.select("div[class=prod-value-holder]").attr("data-delivery-type").equals("ROCKET_DELIVERY")) {
            detailInfo.setRocketDelivery(true);
        }

        String deliCondition = deliveryDoc.select("div[class=prod-value-holder]").attr("data-charge-type");
        detailInfo.setFreeDelivery(CoupangDeliveryType.valueOf(deliCondition).isFreeDelivery());
        detailInfo.setConditionalFreeDeli(CoupangDeliveryType.valueOf(deliCondition).isConditionDelivery());


        dftPriceStr = deliveryDoc.select("div[class=prod-value-holder]").attr("data-free-ship-over-amount");
        if (dftPriceStr.indexOf(".") == -1) {
            detailInfo.setFreeDeliveryPrice(Integer.parseInt(dftPriceStr));
        }


        Elements intervalDelElement = deliveryDoc.select("li[class=choose-item subscription-li selected]");
        if (intervalDelElement.size() > 0) {
            detailInfo.setIntervalDelivery(true);
        }

        String etcHTML = restTemplate.getForObject("http://www.coupang.com/vp/products/" + Long.toString(dealNum) + "/vendor-items/"
                + Long.toString(venderItem) + "/etc", String.class);
        Document etcDoc = Jsoup.parse(etcHTML);


        Element deliveryCop = etcDoc.select("table[class=prod-delivery-return-policy-table]").get(0).select("tr").get(1);

        if (deliveryCop.select("th").text().equals("배송사")) {
            detailInfo.setDeliveryCompany(deliveryCop.select("td").text());
        }
        dealInfo.setDetailInfo(detailInfo);
    }
}
