package com.tmoncorp.admin.service.search;

import com.tmoncorp.admin.enums.TmonDeliveryConditionEnum;
import com.tmoncorp.admin.model.DealInfo;
import com.tmoncorp.admin.model.DealSummaryInfo;
import com.tmoncorp.admin.model.TmonDetailInfo;
import com.tmoncorp.admin.util.CrawlingUtil;
import com.tmoncorp.admin.util.StringUtil;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Created by KangSeongGil on 2017. 8. 1..
 */
@Service
public class TmonSearchService implements SearchService {
    private Logger logger;

    public TmonSearchService() {
        logger = Logger.getLogger(TmonSearchService.class.getName());
    }

    @Override
    public List<DealSummaryInfo> getSummaryList(String searchKeyword, String sortType) {
        logger.debug("----------------");
        logger.debug("tmon SummaryList");


        List<DealSummaryInfo> summaryInfoList = new ArrayList();
        CrawlingUtil crawlingUtil = new CrawlingUtil();

        Document doc = crawlingUtil.getSearchResultDocument(searchKeyword, sortType, "TMON");
        Element dealList = doc.select("ul[class=deal_list]").get(0);

        if (Objects.isNull(dealList)) {
            return null;
        }

        Iterator<Element> dealInfo = dealList.getElementsByTag("li").iterator();

        while (dealInfo.hasNext()) {
            Element dealItem = dealInfo.next();

            long dealNum = Long.parseLong(dealItem.select("a").attr("data-deal_srl"));

            Element detailDealInfo = dealItem.select("a").get(0);
            String dealUrl = detailDealInfo.attr("href");

            String dealName = detailDealInfo.attr("title");
            String dealImageUrl = detailDealInfo.select("img").get(0).attr("src");

            String priceStr = detailDealInfo.select("span[class=deal_item_price]").select("em").get(0).text();
            int dealPrice = StringUtil.removeComma(priceStr);

            DealSummaryInfo tmonItem = new DealSummaryInfo(dealNum, dealName, dealImageUrl, dealPrice, dealUrl);
            summaryInfoList.add(tmonItem);

        }
        logger.debug("search finish");
        logger.debug("----------------");
        return summaryInfoList;
    }

    @Override
    public void setDetailInfo(DealInfo dealInfo) throws Exception {
        logger.debug("----------------");
        logger.debug("tmon detailInfo");
        logger.debug("dealNum:" + dealInfo.getDealSrl());

        TmonDetailInfo detailInfo = new TmonDetailInfo();
        CrawlingUtil crawlingUtil = new CrawlingUtil();
        String crawlingResult = crawlingUtil.getDynamicDocument(dealInfo.getUrl());
        Document doc = Jsoup.parse(crawlingResult);
        Element detailContent = doc.select("div[id=content]").get(0);
        Elements topLocatInfo = detailContent.select("div[class=deal_detail_wrap]");
        Elements bottomLocatinInfo = detailContent.select("div[class=wrap_deal_display]").select("div[id=md_area]");

        Elements nowPriceElements = detailContent.select("strong[class=now_price]").get(0).children();
        String priceStr = "";
        for (Element priceElement : nowPriceElements) {
            if ((Pattern.matches("^[0-9]+$", priceElement.text()))) {
                priceStr += priceElement.text();
            }
        }

        dealInfo.setPrice(Integer.parseInt(priceStr));
        Element topRightInfo = topLocatInfo.select("div[class=ct_area]").get(0);
        Element imgAreaInfo = topLocatInfo.select("div[class=img_area]").get(0);

        Elements priceOptions = topRightInfo.select("p[class=option_noti]");

        if (priceOptions.size() == 0) {
            detailInfo.setPriceOption("없음");
        } else {
            Iterator<Element> priceOptionIterator = priceOptions.iterator();

            while (priceOptionIterator.hasNext()) {
                Element priceOption = priceOptionIterator.next();
                detailInfo.setPriceOption(priceOption.select("strong").text());
            }
        }

        if (topRightInfo.select("span[class=stock]").select("strong[class=num buycount]").size() > 0) {
            String docStr = doc.toString();
            int start = docStr.indexOf("countUpTo"), end = start + 1;
            boolean flag = true;

            while (flag) {
                if (docStr.charAt(end) == ')') {
                    flag = false;
                } else if (docStr.charAt(end) == '(') {
                    start = end;
                }
                end++;
            }

            String subString = docStr.substring(start + 1, end - 1);
            detailInfo.setSaleVolume(Long.parseLong(subString));
        }


        Elements reviewResult = imgAreaInfo.select("div[class=review_result]");

        Elements starPointElements = reviewResult.select("dl").select("span");

        if (starPointElements.size() == 0) {
            detailInfo.setItemReviewScore(0.0);
            detailInfo.setDeliveryReviewScore(0.0);
        } else if (starPointElements.size() > 0) {
            String scoreStr = starPointElements.get(0).text();
            scoreStr = scoreStr.replaceAll("점", "");
            double convertingVal = Double.parseDouble(scoreStr);
            detailInfo.setItemReviewScore(convertingVal);

            if (starPointElements.size() == 2) {
                scoreStr = starPointElements.get(1).text();
                scoreStr = scoreStr.replaceAll("점", "");
                convertingVal = Double.parseDouble(scoreStr);
                detailInfo.setDeliveryReviewScore(convertingVal);
            }
        }

        Elements reviewAmountElement = reviewResult.select("a").select("em");

        if (reviewAmountElement.size() > 0) {
            String reviewAmount = reviewAmountElement.get(0).text();
            detailInfo.setReviewAmount(Long.parseLong(reviewAmount));
        }

        //배달정보
        Elements buyInfo = bottomLocatinInfo.select("div[class=buybefore_info_area]");
        Elements deliveryConditionInfoList = buyInfo.select("div[class=ct_deal_condition min_h1]").select("div[class=menu]").select("li");

        if (deliveryConditionInfoList.size() == 0) {
            detailInfo.setDeliveryCompany("없음");
            detailInfo.setDeliveryPrice(0);
            detailInfo.setFreeDeliveryPrice(0);
            detailInfo.setFreeDelivery(false);
            detailInfo.setSendToday(false);
        } else {
            Iterator<Element> deliveryConditionInfoIter = deliveryConditionInfoList.iterator();

            while (deliveryConditionInfoIter.hasNext()) {
                Element deliveryConditionInfo = deliveryConditionInfoIter.next();
                String deliveryCondition = deliveryConditionInfo.select("a").attr("class");
                TmonDeliveryConditionEnum.valueOf(deliveryCondition.toUpperCase()).deliverySetting(detailInfo);
            }
        }

        Element deliveryInfo = buyInfo.select("div[id=useInfoArea]").select("div[id=useInfo2]").get(0);
        String decider = deliveryInfo.select("h4[id=useinfo_title2]").text();

        if (decider.equals("배송/환불정보")) {
            Elements deliveryInfoList = deliveryInfo.select("li");
            detailInfo.setDeliveryCompany(deliveryInfoList.get(0).text().replaceAll("택배사 : ", ""));

            if (detailInfo.getFreeDelivery()) {
                detailInfo.setDeliveryPrice(0);
                detailInfo.setFreeDeliveryPrice(0);
            } else {
                String priceInfoStr;
                if (deliveryInfoList.size() > 1) {
                    priceInfoStr = deliveryInfoList.get(1).text();
                } else {
                    priceInfoStr = deliveryInfo.select("div").get(0).text();
                    int startIdx = priceInfoStr.indexOf("배송비");
                    int endIdx = priceInfoStr.lastIndexOf("원");
                    logger.debug(priceInfoStr.length());
                    priceInfoStr = priceInfoStr.substring(startIdx,endIdx+1);
                }
                String[] split = priceInfoStr.split(" ");
                String price;

                if (split.length > 3) {
                    price = split[3].replace("원", "");
                } else {
                    price = split[2].replace("원", "");
                }

                price = price.replace("(", "");
                detailInfo.setDeliveryPrice(StringUtil.removeComma(price));

                if (split.length > 5 && split[5].indexOf("원") > -1) {
                    price = split[5].replace("원", "");
                    detailInfo.setFreeDeliveryPrice(StringUtil.removeComma(price));
                }
            }
        }
        logger.debug("종료");
        logger.debug("----------------");
        dealInfo.setDetailInfo(detailInfo);
    }
}
