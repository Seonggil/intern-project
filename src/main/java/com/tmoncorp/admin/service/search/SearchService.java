package com.tmoncorp.admin.service.search;
import com.tmoncorp.admin.model.DealInfo;
import org.jsoup.nodes.Document;
import java.util.List;

/**
 * Created by KangSeongGil on 2017. 8. 1..
 */
public interface SearchService {
   List<?> getSummaryList (String searchKeyword, String sortType);
   void setDetailInfo(DealInfo dealInfo) throws Exception;
}
