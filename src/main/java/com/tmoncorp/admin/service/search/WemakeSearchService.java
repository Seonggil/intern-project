package com.tmoncorp.admin.service.search;


import com.tmoncorp.admin.model.*;
import com.tmoncorp.admin.util.CrawlingUtil;
import com.tmoncorp.admin.util.StringUtil;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Created by KangSeongGil on 2017. 8. 1..
 */
@Service
public class WemakeSearchService implements SearchService {
    private Logger logger;

    public WemakeSearchService() {
        logger = Logger.getLogger(WemakeSearchService.class.getName());
    }

    @Override
    public List<DealSummaryInfo> getSummaryList(String searchKeyword, String sortType) {
        logger.debug("--------------");
        logger.debug("wemakeSummaryList");

        List<DealSummaryInfo> itemList = new ArrayList();
        CrawlingUtil crawlingUtil = new CrawlingUtil();

        Document doc = crawlingUtil.getSearchResultDocument(searchKeyword, sortType, "WEMAKE");
        Element dealList = doc.select("ul[id=search_deal_area]").get(0);

        if (Objects.isNull(dealList)) {
            return null;
        }

        Iterator<Element> dealInfo = dealList.getElementsByTag("li").iterator();
        while (dealInfo.hasNext()) {
            Element dealItem = dealInfo.next();
            long dealNum = Long.parseLong(dealItem.attr("deal_id"));

            Element detailDealInfo = dealItem.select("a").get(0);
            String dealUrl = detailDealInfo.attr("href");

            String dealImgUrl = detailDealInfo.select("span[class = box_thumb]").select("img").attr("data-original");
            String beforConvertingPrice = detailDealInfo.select("span[class=price]").select("span[class=sale]").text();

            int dealPrice;
            if (beforConvertingPrice.length() > 1) {
                int splitIdx = beforConvertingPrice.indexOf("원");
                beforConvertingPrice = beforConvertingPrice.substring(0, splitIdx);
                dealPrice = StringUtil.removeComma(beforConvertingPrice);
            } else {
                dealPrice = 0;
            }

            String dealName = detailDealInfo.select("span[class=box_desc]").select("strong[class=tit_desc]").text();

            DealSummaryInfo item = new DealSummaryInfo(dealNum, dealName, dealImgUrl, dealPrice, dealUrl);
            itemList.add(item);
        }

        logger.debug("search finish");
        logger.debug("----------------");
        return itemList;
    }

    @Override
    public void setDetailInfo(DealInfo dealInfo) throws Exception {

        logger.debug("--------------");
        logger.debug("wemake detailInfo");
        logger.debug("dealNum:"+dealInfo.getDealSrl());
        logger.debug("--------------");

        WemakePriceDetailInfo detailInfo = new WemakePriceDetailInfo();
        CrawlingUtil crawlingUtil = new CrawlingUtil();
        String crawlingResult = crawlingUtil.getDynamicDocument(dealInfo.getUrl());
        Document doc = Jsoup.parse(crawlingResult);

        Element dealInfoEmt = doc.select("div[class=deal_info]").get(0);
        String priceStr=dealInfoEmt.select("li[class=sale]").select("span[class=num]").text();
        dealInfo.setPrice(StringUtil.removeComma(priceStr));


        String saleVolume = dealInfoEmt.select("div[class=buy]").select("strong[class=num]").text();
        detailInfo.setSaleVolume(StringUtil.removeCommaByLong(saleVolume));

        Elements reviewElement = dealInfoEmt.select("div[class=review-rating-box clearfix]").select("div[class=average]");

        if (reviewElement.size() > 0) {
            detailInfo.setReviewAmount(Long.parseLong(reviewElement.select("span[class=num]").select("em").text()));
            detailInfo.setItemReviewScore(Long.parseLong(reviewElement.select("div[class=star]").select("strong[class=grage]").text()));
        }

        Elements deliveryElements = doc.select("div[class=contents-detail]").select("div[class=cd_chk_info]").select("div[class=chk_info_txt fst]");

        //배송가격 정보가 있을때
        if (deliveryElements.size() > 0) {
            Elements extraDeliveryElements = deliveryElements.select("ul[class=list_gray_dot]").select("li");
            deliveryElements = deliveryElements.select("li[class=ir_ico png_alpha]");
            String deliCorp = deliveryElements.get(1).text();
            deliCorp = deliCorp.replaceAll("택배사: ", "");
            detailInfo.setDeliveryCompany(deliCorp);

            String deliPrice = deliveryElements.get(2).text();
            deliPrice = deliPrice.replaceAll("배송비: ", "");

            if (deliPrice.indexOf("무료배송") > -1 && deliPrice.indexOf("이상") == -1) {
                detailInfo.setFreeDelivery(true);
                detailInfo.setConditionalFreeDeli(false);
                detailInfo.setFreeDeliveryPrice(0);
            } else if (deliPrice.indexOf("무료배송") > -1 && deliPrice.indexOf("이상") > 0) {
                detailInfo.setFreeDelivery(false);
                detailInfo.setConditionalFreeDeli(true);

                String tmp[] = deliPrice.split(" ");

                if (tmp[0].equals("유료")) {
                    tmp[1] = tmp[1].replace("원", "");
                    detailInfo.setDeliveryPrice(StringUtil.removeComma(tmp[1]));

                    tmp[3] = tmp[3].replace("원", "");
                    detailInfo.setFreeDeliveryPrice(StringUtil.removeComma(tmp[1]));

                } else {
                    tmp[0] = tmp[0].replace("원", "");
                    detailInfo.setFreeDeliveryPrice(StringUtil.removeComma(tmp[0]));

                    String regularDeliveyPrice = extraDeliveryElements.get(7).text();
                    int bracketStartIdx, bracketEndIdx;

                    bracketStartIdx = regularDeliveyPrice.indexOf("(");
                    bracketEndIdx = regularDeliveyPrice.indexOf(")");
                    regularDeliveyPrice = regularDeliveyPrice.substring(bracketStartIdx, bracketEndIdx).replaceAll("원", "");
                    tmp = regularDeliveyPrice.split(" ");

                    detailInfo.setDeliveryPrice(StringUtil.removeComma(tmp[tmp.length - 1]));
                }

            } else {
                String tmp[] = deliPrice.split(" ");
                tmp[0] = tmp[0].replace("원", "");
                detailInfo.setDeliveryPrice(StringUtil.removeComma(tmp[0]));
            }
            detailInfo.setDeliveryCompany(deliCorp);

            if (deliveryElements.get(deliveryElements.size() - 1).text().indexOf("무료반품") > -1) {
                detailInfo.setFreeReturn(true);
            }
        }
        dealInfo.setDetailInfo(detailInfo);
    }
}
