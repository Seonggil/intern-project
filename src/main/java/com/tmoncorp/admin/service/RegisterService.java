package com.tmoncorp.admin.service;

import com.tmoncorp.admin.VO.RegisterVO;
import com.tmoncorp.admin.model.DealInfo;
import com.tmoncorp.admin.model.DetailSearchDataSet;
import com.tmoncorp.admin.repository.RegistRepository;
import com.tmoncorp.admin.service.search.SearchService;
import com.tmoncorp.admin.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * Created by KangSeongGil on 2017. 7. 26..
 */
@Service
public class RegisterService {

    @Autowired
    private RegistRepository registRepository;


    private Logger logger = Logger.getLogger(SearchService.class.getName());


    @Transactional(rollbackFor = Exception.class)
    public Boolean saveInitDealList(DetailSearchDataSet detailSearchDataSet) throws Exception {
        StringUtil stringUtil = new StringUtil();
        DealInfo baseDealInfo = detailSearchDataSet.getBaseItem();
        List<DealInfo> selectedDealInfoList = detailSearchDataSet.getSelectedItems();
        String jsonData;

        if (Objects.isNull(registRepository)) {
            return false;
        }

        RegisterVO registerVO = new RegisterVO(0, detailSearchDataSet.getTitle(), detailSearchDataSet.getPw()
                , detailSearchDataSet.getSelectedItems().size() + 1);
        logger.debug("저장과정2");

        registRepository.insertInitInfo(registerVO);
        int seqNum = registerVO.getSequenceNum();

        logger.debug("시퀀스 넘버" + seqNum);

        jsonData = stringUtil.makeJsonStringData(baseDealInfo.getDetailInfo());
        baseDealInfo.setJsonDetailInfoData(jsonData);

        for (DealInfo dealInfo : selectedDealInfoList) {
            jsonData = stringUtil.makeJsonStringData(dealInfo.getDetailInfo());
            dealInfo.setJsonDetailInfoData(jsonData);
        }

        registRepository.insertDealListSet(seqNum, detailSearchDataSet.getBaseItem(), detailSearchDataSet.getSelectedItems());

        return true;
    }

}
