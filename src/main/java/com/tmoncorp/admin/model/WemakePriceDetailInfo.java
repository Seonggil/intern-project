package com.tmoncorp.admin.model;

/**
 * Created by KangSeongGil on 2017. 7. 24..
 */
public class WemakePriceDetailInfo extends DetailInfo {
    private long saleVolume;

    public long getSaleVolume() {
        return saleVolume;
    }

    public void setSaleVolume(long saleVolume) {
        this.saleVolume = saleVolume;
    }
}
