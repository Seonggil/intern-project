package com.tmoncorp.admin.model;

import java.util.List;

/**
 * Created by k_seonggil on 2017-07-20.
 */
public class DetailSearchDataSet {

    private DealInfo baseItem;
    private List<DealInfo> selectedItems;
    private String title;
    private String pw;

    public DetailSearchDataSet() {
    }

    public DetailSearchDataSet(DealInfo baseItem, List<DealInfo> selectedItems) {
        this.baseItem = baseItem;
        this.selectedItems = selectedItems;
    }

    public DealInfo getBaseItem() {
        return baseItem;
    }

    public void setBaseItem(DealInfo baseItem) {
        this.baseItem = baseItem;
    }

    public List<DealInfo> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<DealInfo> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }
}
