package com.tmoncorp.admin.model;

import com.tmoncorp.admin.model.DealSummaryInfo;

/**
 * Created by k_seonggil on 2017-07-12.
 */
public class CoupangItem extends DealSummaryInfo {

    private int rocketPrice;
    private int intervalPrice;

    public CoupangItem(long dealSrl, String name, String imageUrl, int price, String url,int rocketPrice,int intervalPrice) {
        super(dealSrl, name, imageUrl, price, url);
        this.rocketPrice = rocketPrice;
        this.intervalPrice = intervalPrice;
    }

    public int getRocketPrice() {
        return rocketPrice;
    }

    public void setRocketPrice(int rocketPrice) {
        this.rocketPrice = rocketPrice;
    }

    public int getIntervalPrice() {
        return intervalPrice;
    }

    public void setIntervalPrice(int intervalPrice) {
        this.intervalPrice = intervalPrice;
    }
}