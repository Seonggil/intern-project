package com.tmoncorp.admin.model;

/**
 * Created by k_seonggil on 2017-07-20.
 */
public class DealSummaryInfo {
    private long dealSrl;
    private String name;
    private String imageUrl;
    private int price;
    private String url;

    public DealSummaryInfo() {
    }

    public DealSummaryInfo(long dealSrl, String name, String imageUrl, int price, String url) {
        this.dealSrl = dealSrl;
        this.name = name;
        this.imageUrl = imageUrl;
        this.price = price;
        this.url = url;
    }

    public long getDealSrl() {
        return dealSrl;
    }

    public void setDealSrl(long dealSrl) {
        this.dealSrl = dealSrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
