package com.tmoncorp.admin.model;

/**
 * Created by k_seonggil on 2017-07-20.
 */
public class DealInfo extends DealSummaryInfo {
    private String corpName;
    private DetailInfo detailInfo;
    private String jsonDetailInfoData;


    public DealInfo() {
    }

    public DealInfo(String corpName, DetailInfo detailInfo, String jsonDetailInfoData) {
        this.corpName = corpName;
        this.detailInfo = detailInfo;
        this.jsonDetailInfoData = jsonDetailInfoData;
    }

    public DealInfo(long dealSrl, String name, String imageUrl, int price, String url, String corpName, DetailInfo detailInfo, String jsonDetailInfoData) {
        super(dealSrl, name, imageUrl, price, url);
        this.corpName = corpName;
        this.detailInfo = detailInfo;
        this.jsonDetailInfoData = jsonDetailInfoData;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public DetailInfo getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(DetailInfo detailInfo) {
        this.detailInfo = detailInfo;
    }

    public String getJsonDetailInfoData() {
        return jsonDetailInfoData;
    }

    public void setJsonDetailInfoData(String jsonDetailInfoData) {
        this.jsonDetailInfoData = jsonDetailInfoData;
    }
}
