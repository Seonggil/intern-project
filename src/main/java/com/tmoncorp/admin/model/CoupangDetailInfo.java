package com.tmoncorp.admin.model;

/**
 * Created by KangSeongGil on 2017. 7. 23..
 */
public class CoupangDetailInfo extends DetailInfo {
    private boolean rocketDelivery;
    private boolean intervalDelivery;

    public CoupangDetailInfo() {
        this.rocketDelivery = false;
        intervalDelivery = false;
    }

    public boolean isRocketDelivery() {
        return rocketDelivery;
    }

    public void setRocketDelivery(boolean rocketDelivery) {
        this.rocketDelivery = rocketDelivery;
    }

    public boolean isIntervalDelivery() {
        return intervalDelivery;
    }

    public void setIntervalDelivery(boolean intervalDelivery) {
        this.intervalDelivery = intervalDelivery;
    }
}
