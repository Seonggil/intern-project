package com.tmoncorp.admin.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by KangSeongGil on 2017. 7. 21..
 */
public class TmonDetailInfo extends DetailInfo {

    private long saleVolume;
    private List<String> priceOption;
    private double deliveryReviewScore;
    private boolean dayChoiceDelivery;
    private boolean sendToday;
    private boolean superMartDelivery;
    private boolean responseDelivery;


    public TmonDetailInfo() {
        responseDelivery = false;
    }

    public List<String> getPriceOption() {
        return priceOption;
    }

    public void setPriceOption(String option) {
        if (Objects.isNull(priceOption)) {
            priceOption = new ArrayList();
        }
        priceOption.add(option);
    }

    public double getDeliveryReviewScore() {
        return deliveryReviewScore;
    }

    public void setDeliveryReviewScore(double deliveryReviewScore) {
        this.deliveryReviewScore = deliveryReviewScore;
    }

    public long getSaleVolume() {
        return saleVolume;
    }

    public void setSaleVolume(long saleVolume) {
        this.saleVolume = saleVolume;
    }

    public boolean getDayChoiceDelivery() {
        return dayChoiceDelivery;
    }

    public void setDayChoiceDelivery(boolean dayChoiceDelivery) {
        this.dayChoiceDelivery = dayChoiceDelivery;
    }

    public boolean getSendToday() {
        return sendToday;
    }

    public void setSendToday(Boolean sendToday) {
        this.sendToday = sendToday;
    }

    public boolean isResponseDelivery() {
        return responseDelivery;
    }

    public void setResponseDelivery(boolean responseDelivery) {
        this.responseDelivery = responseDelivery;
    }

    public boolean getSuperMartDelivery() {
        return superMartDelivery;
    }

    public void setSuperMartDelivery(Boolean superMartDelivery) {
        this.superMartDelivery = superMartDelivery;
    }


}
