package com.tmoncorp.admin.model;


/**
 * Created by KangSeongGil on 2017. 7. 21..
 */
public class DetailInfo {

    private double itemReviewScore;
    private String deliveryCompany;
    private int deliveryPrice;
    private boolean freeDelivery;
    private boolean conditionalFreeDeli;
    private boolean freeReturn;
    private int freeDeliveryPrice;
    private String seller;
    private long reviewAmount;


    public double getItemReviewScore() {
        return itemReviewScore;
    }

    public void setItemReviewScore(double itemReviewScore) {
        this.itemReviewScore = itemReviewScore;
    }

    public String getDeliveryCompany() {
        return deliveryCompany;
    }

    public void setDeliveryCompany(String deliveryCompany) {
        this.deliveryCompany = deliveryCompany;
    }

    public Boolean getFreeReturn() {
        return freeReturn;
    }

    public void setFreeReturn(Boolean freeReturn) {
        this.freeReturn = freeReturn;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public boolean getFreeDelivery() {
        return freeDelivery;
    }

    public void setFreeDelivery(Boolean freeDelivery) {
        this.freeDelivery = freeDelivery;
    }

    public int getFreeDeliveryPrice() {
        return freeDeliveryPrice;
    }

    public void setFreeDeliveryPrice(int freeDeliveryPrice) {
        this.freeDeliveryPrice = freeDeliveryPrice;
    }

    public int getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(int deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public long getReviewAmount() {
        return reviewAmount;
    }

    public void setReviewAmount(Long reviewAmount) {
        this.reviewAmount = reviewAmount;
    }

    public boolean getConditionalFreeDeli() {
        return conditionalFreeDeli;
    }

    public void setConditionalFreeDeli(Boolean conditionalFreeDeli) {
        this.conditionalFreeDeli = conditionalFreeDeli;
    }
}
