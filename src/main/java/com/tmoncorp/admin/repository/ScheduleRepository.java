package com.tmoncorp.admin.repository;

import com.tmoncorp.admin.VO.DealVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by KangSeongGil on 2017. 7. 27..
 */
@Repository
public interface ScheduleRepository {
    List<DealVO> selectResearchDealList() throws Exception;

    int insertDealListSet(@Param("dealVOList") List<DealVO> dealVOList);
}
