package com.tmoncorp.admin.repository;

import com.tmoncorp.admin.VO.RegisterVO;
import com.tmoncorp.admin.model.DealInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by KangSeongGil on 2017. 7. 25..
 */

@Repository
public interface RegistRepository {
    int insertInitInfo(@Param("registerVO") RegisterVO registerVO) throws Exception;

    int insertDealListSet(@Param("seqNo") int SeqNo, @Param("baseDeal") DealInfo baseDeal, @Param("selectedDealList") List<DealInfo> selectedDealList) throws Exception;
}
