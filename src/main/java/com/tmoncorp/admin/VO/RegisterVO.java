package com.tmoncorp.admin.VO;

/**
 * Created by KangSeongGil on 2017. 7. 26..
 */
public class RegisterVO {
    int sequenceNum;
    String title;
    String pw;
    int dealAmount;


    public RegisterVO() {

    }

    public RegisterVO(int sequenceNum, String title, String pw, int dealAmount) {
        this.sequenceNum = sequenceNum;
        this.title = title;
        this.pw = pw;
        this.dealAmount = dealAmount;
    }

    public int getSequenceNum() {
        return sequenceNum;
    }

    public void setSequenceNum(int sequenceNum) {
        this.sequenceNum = sequenceNum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public int getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(int dealAmount) {
        this.dealAmount = dealAmount;
    }
}
