package com.tmoncorp.admin.scheduler;

import com.tmoncorp.admin.VO.DealVO;
import com.tmoncorp.admin.model.DealInfo;
import com.tmoncorp.admin.repository.ScheduleRepository;
import com.tmoncorp.admin.service.search.CoupangSearchService;
import com.tmoncorp.admin.service.search.SearchService;
import com.tmoncorp.admin.service.search.TmonSearchService;
import com.tmoncorp.admin.service.search.WemakeSearchService;
import com.tmoncorp.admin.util.CrawlingUtil;
import com.tmoncorp.admin.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * Created by KangSeongGil on 2017. 7. 26..
 */
@Component
public class ResearchScheduler {

    @Autowired
    private ScheduleRepository scheduleRepository;

    private Map<Object, SearchService> allocateService() {
        Map<Object, SearchService> serviceMap = new HashMap();
        serviceMap.put("티몬", new TmonSearchService());
        serviceMap.put("쿠팡", new CoupangSearchService());
        serviceMap.put("위메프", new WemakeSearchService());

        return serviceMap;
    }

    @Transactional
    @Scheduled(cron = "0 0 */1 * * *")
    public void updateDealList() {
        Logger logger = Logger.getLogger(ResearchScheduler.class.getName());
        List<DealVO> dealVOList = new ArrayList();
        DealInfo dealInfo;
        String jsonStrData;
        Map<Object, SearchService> serviceMap = allocateService();
        List<Integer> errRegitNo = new ArrayList();

        try {
            dealVOList = scheduleRepository.selectResearchDealList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<DealInfo> dealInfoList = new ArrayList();

        for (DealVO dealVO : dealVOList) {
            dealInfo = new DealInfo();
            dealInfo.setCorpName(dealVO.getCorpName());
            dealInfo.setDealSrl(dealVO.getDealSrl());
            dealInfo.setName(dealVO.getName());
            dealInfo.setImageUrl(dealVO.getImageUrl());
            dealInfo.setUrl(dealVO.getUrl());
            dealInfo.setPrice(dealVO.getPrice());

            SearchService searchService = serviceMap.get(dealInfo.getCorpName());;

            try {
                searchService.setDetailInfo(dealInfo);
            } catch (Exception e) {
                logger.error("update error!! corpName : " + dealInfo.getCorpName() + "dealSrl" + dealInfo.getDealSrl());
            }
            dealInfoList.add(dealInfo);
        }

        for (int i = 0; i < dealInfoList.size(); i++) {
            DealInfo dealInfoTmp = dealInfoList.get(i);
            dealVOList.get(i).setPrice(dealInfoTmp.getPrice());

            try {
                if(!Objects.isNull(dealInfoTmp.getDetailInfo())) {
                    jsonStrData = StringUtil.makeJsonStringData(dealInfoTmp.getDetailInfo());
                }else {
                    jsonStrData = dealVOList.get(i).getJsonDetailInfoData();
                }
            } catch (Exception e) {
                jsonStrData =  dealVOList.get(i).getJsonDetailInfoData();
            }

            dealVOList.get(i).setJsonDetailInfoData(jsonStrData);
            dealVOList.get(i).setUpdateAmount(dealVOList.get(i).getUpdateAmount() + 1);
        }

        try {
            scheduleRepository.insertDealListSet(dealVOList);
            logger.debug("주기 성공");
        } catch (Exception e) {
            logger.debug("주기 error");
        }
    }
}
