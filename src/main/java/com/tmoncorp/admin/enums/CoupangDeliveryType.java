package com.tmoncorp.admin.enums;

/**
 * Created by KangSeongGil on 2017. 7. 24..
 */
public enum CoupangDeliveryType {
    FREE(true, false),
    CONDITIONAL(false, true),
    NOT_FREE(false, false);

    private boolean freeDelivery;
    private boolean conditionDelivery;

    CoupangDeliveryType(boolean freeDelivery, boolean conditionDelivery) {
        this.freeDelivery = freeDelivery;
        this.conditionDelivery = conditionDelivery;
    }

    public boolean isFreeDelivery() {
        return freeDelivery;
    }

    public boolean isConditionDelivery() {
        return conditionDelivery;
    }
}
