package com.tmoncorp.admin.enums;

import com.tmoncorp.admin.model.TmonDetailInfo;
import com.tmoncorp.admin.model.DetailInfo;

/**
 * Created by KangSeongGil on 2017. 7. 21..
 */
public enum TmonDeliveryConditionEnum {

    TODAY {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            TmonDetailInfo tmonDetailInfo = (TmonDetailInfo) detailInfo;
            tmonDetailInfo.setSendToday(true);
            tmonDetailInfo.setSuperMartDelivery(false);
        }
    }, NEXTDAY {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            TmonDetailInfo tmonDetailInfo = (TmonDetailInfo) detailInfo;
            tmonDetailInfo.setSendToday(false);
            tmonDetailInfo.setSuperMartDelivery(false);
        }
    }, RETURN {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            detailInfo.setFreeReturn(true);
        }
    }, FREE {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            detailInfo.setFreeDelivery(true);
            detailInfo.setConditionalFreeDeli(false);
        }
    }, CONDITIONAL {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            detailInfo.setConditionalFreeDeli(true);
            detailInfo.setFreeDelivery(false);
        }
    }, MART_CONDITIONAL {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            detailInfo.setConditionalFreeDeli(true);
            detailInfo.setFreeDelivery(false);
        }
    }, RESP_DELIVERY {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            TmonDetailInfo tmonDetailInfo = (TmonDetailInfo) detailInfo;
            tmonDetailInfo.setResponseDelivery(true);
        }
    }, CHOICE {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            TmonDetailInfo tmonDetailInfo = (TmonDetailInfo) detailInfo;
            tmonDetailInfo.setDayChoiceDelivery(true);
            tmonDetailInfo.setSendToday(true);
        }
    }, SUPER_DELIVERY {
        @Override
        public void deliverySetting(DetailInfo detailInfo) {
            TmonDetailInfo tmonDetailInfo = (TmonDetailInfo) detailInfo;
            tmonDetailInfo.setSuperMartDelivery(true);
            tmonDetailInfo.setSendToday(false);
        }
    };

    TmonDeliveryConditionEnum() {

    }

    public abstract void deliverySetting(DetailInfo detailInfo);
}
