package com.tmoncorp.admin.enums;

/**
 * Created by k_seonggil on 2017-07-20.
 */
public enum CoperationSorterEnum {
    COUPANG_FAVORITE("&sorter=scoreDesc"), COUPANG_OPEN("&sorter=latestAsc"), COUPANG_CHEAP("&sorter=salePriceAsc"), COUPANG_EXPENSIVE("&sorter=salePriceDesc"),
    WEMAKE_FAVORITE("&sort=favorite"), WEMAKE_OPEN("&sort=open"), WEMAKE_CHEAP("&sort=cheap"), WEMAKE_EXPENSIVE("&sort=expensive"),
    TMON_FAVORITE(""), TMON_OPEN(""), TMON_CHEAP(""), TMON_EXPENSIVE("");
    private String sorter;

    CoperationSorterEnum(String arg) {
        this.sorter = arg;
    }

    public String getSorter() {
        return sorter;
    }

}
