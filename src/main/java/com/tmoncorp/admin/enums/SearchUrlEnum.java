package com.tmoncorp.admin.enums;

/**
 * Created by k_seonggil on 2017-07-20.
 */
public enum SearchUrlEnum {

    TMON("http://search.ticketmonster.co.kr/search/?keyword="),
    COUPANG("http://www.coupang.com/np/search?component=&q="),
    WEMAKE("http://search.wemakeprice.com/search?search_keyword=");

    private String searchUrl;

    SearchUrlEnum(String arg) {
        this.searchUrl = arg;
    }

    public String getSearchUrl() {
        return searchUrl;
    }
}
