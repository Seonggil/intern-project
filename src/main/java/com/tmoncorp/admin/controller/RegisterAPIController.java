package com.tmoncorp.admin.controller;

import com.tmoncorp.admin.model.DealInfo;
import com.tmoncorp.admin.model.DetailInfo;
import com.tmoncorp.admin.model.DetailSearchDataSet;
import com.tmoncorp.admin.service.RegisterService;
import com.tmoncorp.admin.service.search.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by KangSeongGil on 2017. 7. 26..
 */
@RestController
@RequestMapping("/api/register")
public class RegisterAPIController {

    @Autowired
    RegisterService registerService;
    @Autowired
    SearchServiceFactory searchServiceFactory;


    private Logger logger = Logger.getLogger(SearchAPIController.class.getName());


    @RequestMapping(value = "/deal-detail-info", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void startCompareRequest(@RequestBody DetailSearchDataSet rcvDealList) throws Exception {
        SearchService searchService = searchServiceFactory.getProduct("tmon");
        searchService.setDetailInfo(rcvDealList.getBaseItem());
        List<DealInfo> selectedItemList = rcvDealList.getSelectedItems();

        logger.debug("regit base name:"+ rcvDealList.getBaseItem().getName());

        for(DealInfo dealInfo : selectedItemList) {
            searchService = searchServiceFactory.getProduct(dealInfo.getCorpName());
            searchService.setDetailInfo(dealInfo);
        }

        registerService.saveInitDealList(rcvDealList);
    }
}
