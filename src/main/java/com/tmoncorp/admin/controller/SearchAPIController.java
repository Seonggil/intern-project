package com.tmoncorp.admin.controller;

import com.gargoylesoftware.htmlunit.util.UrlUtils;
import com.tmoncorp.admin.service.search.*;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

/**
 * Created by k_seonggil on 2017-07-13.
 */
@RestController
@RequestMapping("/api/search")
public class SearchAPIController {

    @Autowired
    SearchServiceFactory searchServiceFactory;
    private Logger logger = Logger.getLogger(SearchAPIController.class.getName());


    @RequestMapping(value = "/list/{corpName}/{keyword}", method = RequestMethod.GET, produces = "application/json")
    public Map<Object, Object> searchItem(@PathVariable String corpName, @PathVariable String keyword, @RequestParam("sort-type") String sortType) {
        SearchService searchService = searchServiceFactory.getProduct(corpName);

        logger.debug("키워드는" + keyword);

        Map<Object, Object> returnMap = new HashMap();
        returnMap.put("searchCorp", corpName);
        returnMap.put("dealList", searchService.getSummaryList(keyword, sortType));
        returnMap.put("searchValidation", true);
        return returnMap;
    }
}

