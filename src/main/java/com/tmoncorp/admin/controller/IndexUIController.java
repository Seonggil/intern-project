package com.tmoncorp.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created by k_seonggil on 2017-07-10.
 */

@Controller
@RequestMapping("/")
public class IndexUIController {

    @RequestMapping("admin")
    public String viewAdminIndexPage() {
        return "admin";
    }

}
