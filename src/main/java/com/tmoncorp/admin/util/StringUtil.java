package com.tmoncorp.admin.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmoncorp.admin.model.DetailInfo;

/**
 * Created by KangSeongGil on 2017. 7. 25..
 */
public class StringUtil {
    public static int removeComma(String price) throws NumberFormatException {
        price = price.replaceAll(",", "");
        return Integer.parseInt(price);
    }

    public static long removeCommaByLong(String longVal) {
        longVal = longVal.replaceAll(",", "");
        return Long.parseLong(longVal);
    }

    public static String makeJsonStringData(DetailInfo detailInfo) {
        ObjectMapper mapper = new ObjectMapper();

        String jsonData = null;
        try {
            jsonData = mapper.writeValueAsString(detailInfo);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
        return jsonData;
    }
}
