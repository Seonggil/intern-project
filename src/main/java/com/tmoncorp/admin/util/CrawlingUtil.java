
package com.tmoncorp.admin.util;

import com.tmoncorp.admin.enums.CoperationSorterEnum;
import com.tmoncorp.admin.enums.SearchUrlEnum;
import com.tmoncorp.admin.model.DealInfo;
import com.tmoncorp.admin.model.DetailInfo;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Objects;

/**
 * Created by k_seonggil on 2017-07-12.
 */
public class CrawlingUtil {

    Logger logger = Logger.getLogger(CrawlingUtil.class.getName());

    public Document getDocument(String url) {
        Document doc = new Document(url);
        try {
            doc = Jsoup.connect(url).timeout(5000).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Objects.isNull(doc)) {
            return null;
        }

        return doc;
    }

    public Document getSearchResultDocument(String searchKeyword, String sorter, String corpName) {

        try {
            searchKeyword = URLEncoder.encode(searchKeyword, "UTF-8");
            logger.debug("keyword print:"+ URLDecoder.decode(searchKeyword,"UTF-8"));
        } catch (UnsupportedEncodingException e) {
            logger.debug("URL Encoder Error");
        }

        String url = SearchUrlEnum.valueOf(corpName).getSearchUrl() + searchKeyword;

        String sorterCase = corpName + "_" + sorter.toUpperCase();
        url += CoperationSorterEnum.valueOf(sorterCase).getSorter();

        logger.debug("searchUrl : " + url);

        return getDocument(url);
    }

    public String getDynamicDocument(String url) {
        HtmlUnitDriver driver = new HtmlUnitDriver();
        driver.get(url);
        driver.setJavascriptEnabled(true);
        String str = driver.getPageSource();
        return str;
    }
}

