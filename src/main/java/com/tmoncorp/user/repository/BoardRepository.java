package com.tmoncorp.user.repository;

import com.tmoncorp.user.model.Board;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by KangSeongGil on 2017. 7. 28..
 */
@Repository
public interface BoardRepository {
    List<Board> selectBoardInfo(@Param("startIdx") int startIdx, @Param("endIdx") int endIdx);

    int selectRegitAmount();
    void deleteList(@Param("regitNo")int regitNo);
    String selectPassword(@Param("regitNo")int regitNo);
}
