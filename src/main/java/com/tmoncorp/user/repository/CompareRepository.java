package com.tmoncorp.user.repository;


import com.tmoncorp.user.model.DealInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by KangSeongGil on 2017. 7. 28..
 */
@Repository
public interface CompareRepository {
    List<DealInfo> selectBasicDealInfo(@Param("registNo") int registNo);

    List<DealInfo> selectIntegrationInfo(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("regitNo") int regitNo);

    List<DealInfo> selectCorpIntegrationInfo(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("regitNo") int regitNo, @Param("corpName") String corpName);
}
