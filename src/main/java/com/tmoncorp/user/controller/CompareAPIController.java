package com.tmoncorp.user.controller;


import com.tmoncorp.user.service.CompareService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by KangSeongGil on 2017. 7. 28..
 */

@RestController
@RequestMapping("/api/compare")
public class CompareAPIController {
    private Logger logger = Logger.getLogger(CompareAPIController.class.getName());
    @Autowired
    CompareService compareService;

    @RequestMapping(value = "/basic-info", method = RequestMethod.GET, produces = "application/json")
    public Map<Object, Object> getBasicInfo(@RequestParam("regist-no") int registNo) {
        Map<Object, Object> result = new HashMap<>();
        logger.debug("Basic Info registNo:" + registNo);
        return compareService.getBasicInfo(registNo);
    }

    @RequestMapping(value = "/item/tmon/", method = RequestMethod.GET, produces = "application/json")
    public Map<Object, Object> getDetailInfo(@RequestParam("deal-srl") int dealSrl) {
        Map<Object, Object> result = new HashMap<>();
        return result;
    }

    @RequestMapping(value = "/integration", method = RequestMethod.GET, produces = "application/json")
    public Map<Object, Object> getIntegrationInfo(@RequestParam("startDate") long startDate, @RequestParam("endDate") long endDate, @RequestParam("regitNo") int regitNo) {

        LocalDateTime startLocalDate =
                Instant.ofEpochMilli(startDate).plusSeconds(32400)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime().plusHours(9);

        LocalDateTime endLocalDate =
                Instant.ofEpochMilli(endDate).plusSeconds(32400)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime().plusHours(9);

        return compareService.getIntegrationInfo(startLocalDate, endLocalDate, regitNo);
    }

    @RequestMapping(value = "/corpIntegration", method = RequestMethod.GET, produces = "application/json")
    public Map<Object,Object> getCorpIntegrationData (@RequestParam("startDate") long startDate, @RequestParam("endDate") long endDate,
                                                      @RequestParam("regitNo") int registNo,@RequestParam("corpName") String corpName) {
        return compareService.getCorpIntegrationData(startDate,endDate,registNo,corpName);
    }

}
