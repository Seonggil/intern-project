package com.tmoncorp.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by KangSeongGil on 2017. 7. 31..
 */
@Controller
@RequestMapping("/")
public class UserUIController {
    public String redirectRoot() {
        return "user-board";
    }

    @RequestMapping("user-main")
    public String viewBoardPage() {
        return "user-board";
    }

    @RequestMapping("compare-view")
    public String comparePage(@RequestParam("regist-no") int registNo) {
        return "compare-view";
    }
}

