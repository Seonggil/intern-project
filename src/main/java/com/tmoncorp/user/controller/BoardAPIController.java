package com.tmoncorp.user.controller;

import com.tmoncorp.user.service.BoardService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/board")
public class BoardAPIController {
    @Autowired
    BoardService boardService;
    private Logger logger = Logger.getLogger(BoardService.class.getName());


    @RequestMapping(value = "/main", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Object> searchMainBoard(@RequestParam(value = "index", defaultValue = "1") int idx) {
        logger.debug("searchItem");
        Map<String, Object> boardMap = new HashMap();
        boardMap.put("boardInfo", boardService.inquiryMainBoard(idx - 1));
        boardMap.put("index", idx);
        boardMap.put("registAmount", boardService.getRegitAmount());
        logger.debug("registDate:"+ boardMap.get("boardInfo").toString());
        return boardMap;
    }

    @RequestMapping(value = "/list-amount", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Object> getListAmount( ) {
        Map<String, Object> rtnMap = new HashMap();
        rtnMap.put("amount",boardService.getRegitAmount());
        return rtnMap;
    }

    @RequestMapping(value = "/main", method = RequestMethod.DELETE, produces = "application/json")
    public void deleteList(@RequestParam(value = "regist-no") int regitNo) {
        logger.debug("Delete regist No:" + regitNo);
        boardService.deleteList(regitNo);
    }

    @RequestMapping(value = "/checking", method = RequestMethod.POST, produces = "application/json")
    public  Map<Object,Object> checkPassword(@RequestParam(value = "pw") String pw , @RequestParam(value = "regist-no") int regitNo) {
        Map<Object,Object> loginMap = new HashMap();
        boolean validation =boardService.checkPassword(regitNo,pw);
        loginMap.put("loginValidation",validation);
        return loginMap;
    }
}
