package com.tmoncorp.user.model;


import java.util.Date;

/**
 * Created by KangSeongGil on 2017. 7. 28..
 */
public class Board {
    int registNo;
    Date registDT;
    String title;
    int itemAmount;

    public Board() {
    }

    public Board(int registNo, Date registDT, String title, int itemAmount) {
        this.registNo = registNo;
        this.registDT = registDT;
        this.title = title;
        this.itemAmount = itemAmount;
    }

    public int getRegistNo() {
        return registNo;
    }

    public void setRegistNo(int registNo) {
        this.registNo = registNo;
    }

    public Date getRegistDT() {
        return registDT;
    }

    public void setRegistDT(Date registDT) {
        this.registDT = registDT;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(int itemAmount) {
        this.itemAmount = itemAmount;
    }
}
