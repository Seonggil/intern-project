package com.tmoncorp.user.model;

import java.util.Date;

/**
 * Created by KangSeongGil on 2017. 8. 7..
 */
public class TmonDetailInfo {
    private String name;
    private Date updateDT;
    double deliveryReviewScore;
    boolean freeDelivery;
    boolean freeReturn;
    boolean dayChoiceDelivery;
    boolean sendToday;
    boolean superMartDelivery;
    boolean responseDelivery;
    boolean conditionalFreeDeli;
    long saleAmount;

    public TmonDetailInfo() {
    }

    public double getDeliveryReviewScore() {
        return deliveryReviewScore;
    }

    public void setDeliveryReviewScore(double deliveryReviewScore) {
        this.deliveryReviewScore = deliveryReviewScore;
    }

    public long getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(long saleAmount) {
        this.saleAmount = saleAmount;
    }

    public boolean isDayChoiceDelivery() {
        return dayChoiceDelivery;
    }

    public void setDayChoiceDelivery(boolean dayChoiceDelivery) {
        this.dayChoiceDelivery = dayChoiceDelivery;
    }

    public boolean isSendToday() {
        return sendToday;
    }

    public void setSendToday(boolean sendToday) {
        this.sendToday = sendToday;
    }

    public boolean isSuperMartDelivery() {
        return superMartDelivery;
    }

    public void setSuperMartDelivery(boolean superMartDelivery) {
        this.superMartDelivery = superMartDelivery;
    }

    public boolean isResponseDelivery() {
        return responseDelivery;
    }

    public void setResponseDelivery(boolean responseDelivery) {
        this.responseDelivery = responseDelivery;
    }

    public boolean isFreeDelivery() {
        return freeDelivery;
    }

    public void setFreeDelivery(boolean freeDelivery) {
        this.freeDelivery = freeDelivery;
    }

    public boolean isFreeReturn() {
        return freeReturn;
    }

    public void setFreeReturn(boolean freeReturn) {
        this.freeReturn = freeReturn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdateDT() {
        return updateDT;
    }

    public void setUpdateDT(Date updateDT) {
        this.updateDT = updateDT;
    }

    public boolean isConditionalFreeDeli() {
        return conditionalFreeDeli;
    }

    public void setConditionalFreeDeli(boolean conditionalFreeDeli) {
        this.conditionalFreeDeli = conditionalFreeDeli;
    }
}
