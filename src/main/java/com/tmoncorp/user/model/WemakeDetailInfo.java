package com.tmoncorp.user.model;

import java.util.Date;

/**
 * Created by KangSeongGil on 2017. 8. 7..
 */
public class WemakeDetailInfo {
    private String name;
    private Date updateDT;
    long saleAmount;
    boolean freeDelivery;
    boolean conditionalFreeDeli;
    boolean freeReturn ;

    public WemakeDetailInfo() {
    }

    public long getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(long saleAmount) {
        this.saleAmount = saleAmount;
    }

    public boolean isFreeDelivery() {
        return freeDelivery;
    }

    public void setFreeDelivery(boolean freeDelivery) {
        this.freeDelivery = freeDelivery;
    }

    public boolean isConditionalFreeDeli() {
        return conditionalFreeDeli;
    }

    public void setConditionalFreeDeli(boolean conditionalFreeDeli) {
        this.conditionalFreeDeli = conditionalFreeDeli;
    }

    public boolean isFreeReturn() {
        return freeReturn;
    }

    public void setFreeReturn(boolean freeReturn) {
        this.freeReturn = freeReturn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdateDT() {
        return updateDT;
    }

    public void setUpdateDT(Date updateDT) {
        this.updateDT = updateDT;
    }
}
