package com.tmoncorp.user.model;

import java.time.LocalDate;
import java.util.Date;

/**
 * Created by KangSeongGil on 2017. 7. 30..
 */
public class CompareData {
    private long dealSrl;
    private String name;
    private String url;
    private int price;
    private String deliveryType;
    private int deliveryFee;
    private double itemReviewScore;
    private String corpName;
    private Date updateDT;
    private long reviewAmount;

    public CompareData() {
    }

    public long getDealSrl() {
        return dealSrl;
    }

    public void setDealSrl(long dealSrl) {
        this.dealSrl = dealSrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public int getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(int deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public double getItemReviewScore() {
        return itemReviewScore;
    }

    public void setItemReviewScore(double itemReviewScore) {
        this.itemReviewScore = itemReviewScore;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getUpdateDT() {
        return updateDT;
    }

    public void setUpdateDT(Date updateDT) {
        this.updateDT = updateDT;
    }

    public long getReviewAmount() {
        return reviewAmount;
    }

    public void setReviewAmount(long reviewAmount) {
        this.reviewAmount = reviewAmount;
    }
}