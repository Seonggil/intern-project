package com.tmoncorp.user.model;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

import java.time.LocalDate;
import java.util.Date;

/**
 * Created by KangSeongGil on 2017. 7. 28..
 */
@JsonIgnoreProperties({"jsonDetailInfoData"})
public class DealInfo {
    private int dealRegistNo;
    private int registNo;
    private long dealSrl;
    private String name;
    private String imageUrl;
    private int price;
    private String url;
    private String corpName;
    private String jsonDetailInfoData;
    private int reference;
    private int updateAmount;
    private JsonNode detailInfo;
    private Date updateDT;

    public DealInfo() {
    }


    public int getDealRegistNo() {
        return dealRegistNo;
    }

    public void setDealRegistNo(int dealRegistNo) {
        this.dealRegistNo = dealRegistNo;
    }

    public int getRegistNo() {
        return registNo;
    }

    public void setRegistNo(int registNo) {
        this.registNo = registNo;
    }

    public long getDealSrl() {
        return dealSrl;
    }

    public void setDealSrl(long dealSrl) {
        this.dealSrl = dealSrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getJsonDetailInfoData() {
        return jsonDetailInfoData;
    }

    public void setJsonDetailInfoData(String jsonDetailInfoData) {
        this.jsonDetailInfoData = jsonDetailInfoData;
    }

    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public int getUpdateAmount() {
        return updateAmount;
    }

    public void setUpdateAmount(int updateAmount) {
        this.updateAmount = updateAmount;
    }

    public JsonNode getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(JsonNode detailInfo) {
        this.detailInfo = detailInfo;
    }

    public Date getUpdateDT() {
        return updateDT;
    }

    public void setUpdateDT(Date updateDT) {
        this.updateDT = updateDT;
    }
}
