package com.tmoncorp.user.model;

import java.util.Date;

/**
 * Created by KangSeongGil on 2017. 8. 7..
 */
public class CoupangDetailInfo {
    private String name;
    private Date updateDT;
    boolean freeDelivery;
    boolean freeReturn;
    boolean rocketDelivery;
    boolean intervalDelivery;
    boolean conditionalFreeDeli;

    public CoupangDetailInfo() {
    }

    public boolean isFreeDelivery() {
        return freeDelivery;
    }

    public void setFreeDelivery(boolean freeDelivery) {
        this.freeDelivery = freeDelivery;
    }

    public boolean isFreeReturn() {
        return freeReturn;
    }

    public void setFreeReturn(boolean freeReturn) {
        this.freeReturn = freeReturn;
    }

    public boolean isRocketDelivery() {
        return rocketDelivery;
    }

    public void setRocketDelivery(boolean rocketDelivery) {
        this.rocketDelivery = rocketDelivery;
    }

    public boolean isIntervalDelivery() {
        return intervalDelivery;
    }

    public void setIntervalDelivery(boolean intervalDelivery) {
        this.intervalDelivery = intervalDelivery;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdateDT() {
        return updateDT;
    }

    public void setUpdateDT(Date updateDT) {
        this.updateDT = updateDT;
    }

    public boolean isConditionalFreeDeli() {
        return conditionalFreeDeli;
    }

    public void setConditionalFreeDeli(boolean conditionalFreeDeli) {
        this.conditionalFreeDeli = conditionalFreeDeli;
    }
}
