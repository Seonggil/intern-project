package com.tmoncorp.user.enums;

/**
 * Created by KangSeongGil on 2017. 7. 28..
 */
public enum BoardIndexEnum {
    START(10), END(10);

    private int idx;

    BoardIndexEnum(int idx) {
        this.idx = idx;
    }

    public int getStartIdx(int idx) {
        return this.idx * idx;
    }

    public int getEndIdx(int idx) {
        return this.idx + idx;
    }
}
