package com.tmoncorp.user.service;

import com.tmoncorp.user.repository.CompareRepository;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * Created by KangSeongGil on 2017. 8. 7..
 */
public interface CorpIntegrationComp {
    Map<Object,Object> getCorpInteCompData(CompareRepository compareRepository, LocalDateTime startDate, LocalDateTime endDate, int regitNo);
}
