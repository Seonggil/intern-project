package com.tmoncorp.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmoncorp.user.model.CoupangDetailInfo;
import com.tmoncorp.user.model.DealInfo;
import com.tmoncorp.user.repository.CompareRepository;
import com.tmoncorp.user.util.JsonUtil;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by KangSeongGil on 2017. 8. 7..
 */
@Component
public class CoupangIntegrationComp implements CorpIntegrationComp {

    @Override
    public Map<Object, Object> getCorpInteCompData(CompareRepository compareRepository, LocalDateTime startDate, LocalDateTime endDate, int regitNo) {
        List<DealInfo> dealInfoList = compareRepository.selectCorpIntegrationInfo(startDate.toString().replace("T", " "),
                endDate.toString().replace("T", " "), regitNo, "쿠팡");
        if (dealInfoList.size() == 0) {
            return null;
        }

        Map<Object,Object> groupingMap = JsonUtil.makeUpdateGroupingMap(dealInfoList);
        Map<Object,Object> resultMap = new LinkedHashMap();
        int listIdx = -1;

        for (Map.Entry<Object, Object> entry : groupingMap.entrySet()) {
            List<DealInfo> dealGroupList = (List<DealInfo>) entry.getValue();
            List<CoupangDetailInfo> coupangDetailInfoList =  new ArrayList();
            listIdx++;
            for (DealInfo dealInfo : dealGroupList) {
                coupangDetailInfoList.add(mappingDetailInfo(dealInfo));
            }
            resultMap.put("list" + String.valueOf(listIdx),coupangDetailInfoList);
        }

        return resultMap;
    }

    private CoupangDetailInfo mappingDetailInfo(DealInfo dealInfo) {
        CoupangDetailInfo coupangDetailInfo = new CoupangDetailInfo();
        JsonUtil jsonUtil = new JsonUtil();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = null;

        try {
            jsonNode = mapper.readTree(dealInfo.getJsonDetailInfoData());
        } catch (IOException e) {
            e.printStackTrace();
        }

        coupangDetailInfo.setName(dealInfo.getName());
        coupangDetailInfo.setUpdateDT(dealInfo.getUpdateDT());
        coupangDetailInfo.setFreeDelivery(jsonNode.get("freeDelivery").asBoolean());
        coupangDetailInfo.setFreeReturn(jsonNode.get("freeReturn").asBoolean());
        coupangDetailInfo.setRocketDelivery(jsonNode.get("rocketDelivery").asBoolean());
        coupangDetailInfo.setIntervalDelivery(jsonNode.get("intervalDelivery").asBoolean());
        coupangDetailInfo.setConditionalFreeDeli(jsonNode.get("conditionalFreeDeli").asBoolean());

        return coupangDetailInfo;
    }

}
