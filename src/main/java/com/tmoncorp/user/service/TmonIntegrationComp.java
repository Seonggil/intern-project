package com.tmoncorp.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmoncorp.user.model.DealInfo;
import com.tmoncorp.user.model.TmonDetailInfo;
import com.tmoncorp.user.repository.CompareRepository;
import com.tmoncorp.user.util.JsonUtil;
import org.springframework.stereotype.Component;

import java.lang.Object;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;


/**
 * Created by KangSeongGil on 2017. 8. 7..
 */
@Component
public class TmonIntegrationComp implements CorpIntegrationComp {

    @Override
    public Map<Object, Object> getCorpInteCompData(CompareRepository compareRepository, LocalDateTime startDate, LocalDateTime endDate, int regitNo) {
        List<DealInfo> dealInfoList = compareRepository.selectCorpIntegrationInfo(startDate.toString().replace("T", " "),
                endDate.toString().replace("T", " "), regitNo, "티몬");

        if (dealInfoList.size() == 0) {
            return null;
        }

        Map<Object,Object> groupingMap = JsonUtil.makeUpdateGroupingMap(dealInfoList);
        Map<Object,Object> resultMap = new LinkedHashMap();
        int listIdx = -1;

        for (Map.Entry<Object, Object> entry : groupingMap.entrySet()) {
            List<DealInfo> dealGroupList = (List<DealInfo>) entry.getValue();
            List<TmonDetailInfo> tmonDetailInfoList = new ArrayList();
            listIdx++;
            for (DealInfo dealInfo : dealGroupList) {
                tmonDetailInfoList.add(mappingDetailInfo(dealInfo));
            }
            resultMap.put("list" + String.valueOf(listIdx),tmonDetailInfoList);
        }

        return resultMap;
    }

    private TmonDetailInfo mappingDetailInfo(DealInfo dealInfo) {
        TmonDetailInfo tmonDetailInfo = new TmonDetailInfo();
        JsonUtil jsonUtil = new JsonUtil();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = null;

        try {
            jsonNode = mapper.readTree(dealInfo.getJsonDetailInfoData());
        } catch (IOException e) {
            e.printStackTrace();
        }

        tmonDetailInfo.setName(dealInfo.getName());
        tmonDetailInfo.setUpdateDT(dealInfo.getUpdateDT());
        tmonDetailInfo.setDayChoiceDelivery(jsonNode.get("dayChoiceDelivery").asBoolean());
        tmonDetailInfo.setDeliveryReviewScore(jsonNode.get("deliveryReviewScore").asDouble());
        tmonDetailInfo.setFreeDelivery(jsonNode.get("freeDelivery").asBoolean());
        tmonDetailInfo.setFreeReturn(jsonNode.get("freeReturn").asBoolean());
        tmonDetailInfo.setResponseDelivery(jsonNode.get("responseDelivery").asBoolean());
        tmonDetailInfo.setSaleAmount(jsonNode.get("saleVolume").asLong());
        tmonDetailInfo.setSendToday(jsonNode.get("sendToday").asBoolean());
        tmonDetailInfo.setSuperMartDelivery(jsonNode.get("superMartDelivery").asBoolean());
        tmonDetailInfo.setConditionalFreeDeli(jsonNode.get("conditionalFreeDeli").asBoolean());

        return tmonDetailInfo;
    }
}
