package com.tmoncorp.user.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by KangSeongGil on 2017. 8. 7..
 */
@Component
public class CorpIntegrationFactory {
    @Autowired
    TmonIntegrationComp tmonIntegrationComp;
    @Autowired
    CoupangIntegrationComp coupangIntegrationComp;
    @Autowired
    WemakeIntegrationComp wemakeIntegrationComp;

    public CorpIntegrationComp getProduct(String corpName) {
        if ("tmon".equals(corpName)) {
            return tmonIntegrationComp;
        } else if ("coupang".equals(corpName)) {
            return coupangIntegrationComp;
        } else if ("wemake".equals(corpName)) {
            return wemakeIntegrationComp;
        } else {
            return null;
        }
    }
}
