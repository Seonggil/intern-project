package com.tmoncorp.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmoncorp.user.enums.DinstictDealEnum;
import com.tmoncorp.user.model.CompareData;
import com.tmoncorp.user.repository.CompareRepository;
import com.tmoncorp.user.model.DealInfo;
import com.tmoncorp.user.util.JsonUtil;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * Created by KangSeongGil on 2017. 7. 28..
 */
@Service
public class CompareService {
    private final static int STANDARD_FLAG = 1;
    private Logger logger = Logger.getLogger(CompareService.class.getName());

    @Autowired
    CompareRepository compareRepository;
    @Autowired
    CorpIntegrationFactory corpIntegrationFactory;

    /**
     * @param dealInfoList : DB에서 가져온 딜 정보가 담긴 List
     * @return : 기준 딜 정보만 모은 List
     * Description : 기준 딜만 List로 모은다.
     */
    private List<DealInfo> collectingStdDeal(List<DealInfo> dealInfoList) {
        List<DealInfo> stdDealList = new ArrayList();
        int endIdx = 0;

        for (int i = 0; i < dealInfoList.size(); i++) {
            if (dealInfoList.get(i).getReference() == DinstictDealEnum.SELECTED.ordinal()) {
                endIdx = i - 1;
                break;
            }
        }

        if (endIdx > 0) {
            stdDealList.addAll(dealInfoList.subList(0, endIdx));
            for (int i = 0; i <= endIdx; i++) {
                dealInfoList.remove(0);
            }
        } else if (endIdx == 0 && dealInfoList.size() > 0) {
            stdDealList.add(dealInfoList.remove(0));
        } else {
            return null;
        }

        return stdDealList;
    }


    /**
     * @param startDate : 업데이트 시작 기준 시간
     * @param endDate   :  업데이트 끝 기준 시간
     * @param regitNo   : 딜 그룹 등록번호
     *                  Description : 시작시간과 끝시간을 기준으로 정보를 DB에서 가져와 JSON 데이터로 변환
     */
    public Map<Object, Object> getIntegrationInfo(LocalDateTime startDate, LocalDateTime endDate, int regitNo) {
        org.slf4j.Logger server = LoggerFactory.getLogger("server");
        server.debug("time : "+ startDate.toString() +"/"+endDate.toString());
        List<DealInfo> dealInfoList = compareRepository.selectIntegrationInfo(startDate.toString().replace("T", " "),
                endDate.toString().replace("T", " "), regitNo);
        if (dealInfoList.size() == 0) {
            return null;
        }

        Map<Object, Object> groupingDataMap = JsonUtil.makeUpdateGroupingMap(dealInfoList);
        Map<Object, Object> resultMap = new LinkedHashMap();
        int listIdx = -1;

        for (Map.Entry<Object, Object> entry : groupingDataMap.entrySet()) {
            List<DealInfo> dealGroupList = (List<DealInfo>) entry.getValue();
            List<CompareData> mappedData = new ArrayList();
            listIdx++;
            for (DealInfo dealInfo : dealGroupList) {
                mappedData.add(mappingCompareData(dealInfo));
            }
            resultMap.put("list" + String.valueOf(listIdx), mappedData);
        }
        return resultMap;
    }

    /**
     * @param : 검색된 딜정보
     * @return : 딜정보 중 비교에 비교에 필요한 정보들을 모은 model
     * description : 딜 정보에서 비교에 필요한 정보들만 추출
     */
    private   CompareData mappingCompareData(DealInfo dealInfo) throws NullPointerException {
        CompareData compareData = new CompareData();
        JsonUtil jsonUtil = new JsonUtil();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = null;

        try {
            jsonNode = mapper.readTree(dealInfo.getJsonDetailInfoData());
        } catch (IOException e) {
            e.printStackTrace();
        }

        compareData.setDealSrl(dealInfo.getDealSrl());
        compareData.setName(dealInfo.getName());
        compareData.setUrl(dealInfo.getUrl());
        compareData.setPrice(dealInfo.getPrice());
        compareData.setCorpName(dealInfo.getCorpName());
        compareData.setDeliveryFee(jsonNode.get("deliveryPrice").asInt());
        compareData.setDeliveryType(getDeliveryType(jsonNode));
        compareData.setItemReviewScore(jsonNode.get("itemReviewScore").asDouble());
        compareData.setUpdateDT(dealInfo.getUpdateDT());
        compareData.setReviewAmount(jsonNode.get("reviewAmount").asLong());

        return compareData;
    }

    private  String getDeliveryType(JsonNode jsonNode) {
        if (jsonNode.get("freeDelivery").asBoolean()) {
            return "freeDelivery";
        } else if (jsonNode.get("conditionalFreeDeli").asBoolean()) {
            return "conditionalDelivery";
        } else {
            return "notFree";
        }
    }


    /**
     * @param registNo: 딜 그룹 등록번호
     * @return : 기본 딜 정보(최신 업데이트)를 JSON 데이터 형으로 만들기 위해 MAP에 담음
     * Description: 데이터 베이스에서 최신 딜 정보를 가져와 Map 형태로 만든다.
     */
    public Map<Object, Object> getBasicInfo(int registNo) {
        Map<Object, Object> basicInfoMap = new HashMap();

        List<DealInfo> dealInfoList = compareRepository.selectBasicDealInfo(registNo);
        List<CompareData> compareDataList = new ArrayList();

        DealInfo stdDeal = collectingStdDeal(dealInfoList).get(0);
        compareDataList.add(mappingCompareData(stdDeal));

        for (DealInfo dealInfo : dealInfoList) {
            compareDataList.add(mappingCompareData(dealInfo));
        }

        setCompareData(compareDataList);

        basicInfoMap.put("standardDealInfo", compareDataList.remove(0));
        basicInfoMap.put("selectedDealInfo", compareDataList);

        return basicInfoMap;
    }


    private void setCompareData(List<CompareData> compareDataList) {
        CompareData stdData = compareDataList.get(0);
        Iterator<CompareData> compareDataIterator = compareDataList.iterator();

        compareDataIterator.next();

        while (compareDataIterator.hasNext()) {
            CompareData compareData = compareDataIterator.next();
            compareData.setDeliveryFee(compareData.getDeliveryFee() - stdData.getDeliveryFee());
            compareData.setPrice(compareData.getPrice() - stdData.getPrice());
            compareData.setItemReviewScore(compareData.getItemReviewScore() - stdData.getItemReviewScore());
        }
    }

    public Map<Object, Object> getCorpIntegrationData(long startDate, long endDate, int regitNo, String corpName) {
        LocalDateTime startLocalDate =
                Instant.ofEpochMilli(startDate)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime().plusHours(9);

        LocalDateTime endLocalDate =
                Instant.ofEpochMilli(endDate)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime().plusHours(9);

        CorpIntegrationComp corpIntegrationComp = corpIntegrationFactory.getProduct(corpName);
        return corpIntegrationComp.getCorpInteCompData(compareRepository, startLocalDate, endLocalDate, regitNo);
    }


}
