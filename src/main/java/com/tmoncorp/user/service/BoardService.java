package com.tmoncorp.user.service;

import com.tmoncorp.user.enums.BoardIndexEnum;
import com.tmoncorp.user.model.Board;
import com.tmoncorp.user.repository.BoardRepository;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by KangSeongGil on 2017. 7. 28..
 */
@Service
public class BoardService {

    @Autowired
    BoardRepository boardRepository;

    public List<Board> inquiryMainBoard(int idx) {
        BoardIndexEnum.valueOf("START").getStartIdx(idx);
        return boardRepository.selectBoardInfo(BoardIndexEnum.valueOf("START").getStartIdx(idx),
                BoardIndexEnum.valueOf("END").getEndIdx(idx));
    }

    public int getRegitAmount() {
        return boardRepository.selectRegitAmount();
    }

    @Transactional
    public void deleteList(int regitNo) {
        boardRepository.deleteList(regitNo);
    }

    public boolean checkPassword(int regitNo, String pw) {
        String savePW = boardRepository.selectPassword(regitNo);

        if(pw.equals(savePW)) {
            return true;
        }else {
            return false;
        }
    }


}
