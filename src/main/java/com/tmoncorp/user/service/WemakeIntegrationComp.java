package com.tmoncorp.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmoncorp.user.model.DealInfo;
import com.tmoncorp.user.model.WemakeDetailInfo;
import com.tmoncorp.user.repository.CompareRepository;
import com.tmoncorp.user.util.JsonUtil;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by KangSeongGil on 2017. 8. 7..
 */
@Component
public class WemakeIntegrationComp implements CorpIntegrationComp {

    @Override
    public Map<Object, Object> getCorpInteCompData(CompareRepository compareRepository, LocalDateTime startDate, LocalDateTime endDate, int regitNo) {
        List<DealInfo> dealInfoList = compareRepository.selectCorpIntegrationInfo(startDate.toString().replace("T", " "),
                endDate.toString().replace("T", " "), regitNo, "위메프");
        if (dealInfoList.size() == 0) {
            return null;
        }

        Map<Object,Object> groupingMap = JsonUtil.makeUpdateGroupingMap(dealInfoList);
        Map<Object,Object> resultMap = new LinkedHashMap<>();
        int listIdx = -1;

        for (Map.Entry<Object, Object> entry : groupingMap.entrySet()) {
            List<DealInfo> dealGroupList = (List<DealInfo>) entry.getValue();
            List<WemakeDetailInfo> wemakeDetailInfoList = new ArrayList();
            listIdx++;
            for (DealInfo dealInfo : dealGroupList) {
                wemakeDetailInfoList.add(mappingDetailInfo(dealInfo));
            }
            resultMap.put("list" + String.valueOf(listIdx),wemakeDetailInfoList);
        }

        return resultMap;
    }

    private WemakeDetailInfo mappingDetailInfo(DealInfo dealInfo) {
        WemakeDetailInfo wemakeDetailInfo = new WemakeDetailInfo();
        JsonUtil jsonUtil = new JsonUtil();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = null;

        try {
            jsonNode = mapper.readTree(dealInfo.getJsonDetailInfoData());
        } catch (IOException e) {
            e.printStackTrace();
        }

        wemakeDetailInfo.setName(dealInfo.getName());
        wemakeDetailInfo.setUpdateDT(dealInfo.getUpdateDT());
        wemakeDetailInfo.setSaleAmount(jsonNode.get("saleVolume").asLong());
        wemakeDetailInfo.setConditionalFreeDeli(jsonNode.get("conditionalFreeDeli").asBoolean());
        wemakeDetailInfo.setFreeReturn(jsonNode.get("freeReturn").asBoolean());
        wemakeDetailInfo.setFreeDelivery(jsonNode.get("freeDelivery").asBoolean());

        return wemakeDetailInfo;
    }
}
