package com.tmoncorp.user.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmoncorp.user.model.DealInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.lang.Object;
/**
 * Created by KangSeongGil on 2017. 7. 30..
 */
public class JsonUtil {

    public void convertDetailDealString(DealInfo dealInfo) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            dealInfo.setDetailInfo(mapper.readTree(dealInfo.getJsonDetailInfoData()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param dealInfoList
     * @return 그룹핑이 끝난 결과를 맵으로  return
     * description: 업데이트 횟수별로 그룹핑을 한다.
     */
    public static Map<Object,Object> makeUpdateGroupingMap (List<DealInfo> dealInfoList) {
        List<DealInfo> groupMappingList = new ArrayList();
        Map<Object, Object> resultMap = new LinkedHashMap();
        int groupIdx = 0, dealListSize = dealInfoList.size(), updateAmount = dealInfoList.get(0).getUpdateAmount();

        for (int i = 0; i < dealListSize; i++) {
            DealInfo dealInfo = dealInfoList.remove(0);

            if (updateAmount != dealInfo.getUpdateAmount()) {
                resultMap.put(String.valueOf(groupIdx), groupMappingList);
                groupMappingList = new ArrayList();
                updateAmount++;
                groupIdx++;
            }

            groupMappingList.add(dealInfo);

            if(dealListSize - 1 == i) {
                resultMap.put(String.valueOf(groupIdx), groupMappingList);
            }

        }

        return resultMap;
    }

}
