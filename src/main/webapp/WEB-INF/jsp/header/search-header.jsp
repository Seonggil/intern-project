
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="/seonggil/resources/js/search.js"></script>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin">Home</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown open">
                    <li class="dropdown">
                        <a id="presentSearchType" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">검색타입
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                            <li class="searchType" role="presentation"><a>티몬 내 검색</a></li>
                            <li class="searchType" role="presentation"><a>타 홈쇼핑 결과 포함</a></li>
                        </ul>
                    </li>
                </li>
            </ul>
            <div class="form-group navbar-form navbar-center" role="search">
                <input type="text" class="form-control" placeholder="Search" id="search-contents">
                <button type="button" class="btn btn-default" id="nav-search-button">검색</button>
            </div>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>