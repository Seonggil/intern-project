<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container">
    <div class="header" style="padding-top: 15px">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="/user-main">Home</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">비교항목</h3>
    </div>

    <div class="panel panel-default panel-table" id="table-panel" style="padding-top: 15px">
        <div class="panel-body" id="list-table">
            <table class="table table-bordered table-list">
                <thead>
                    <th width="50px" height="30px" >
                        설정
                    </th>
                    <th height="30px">
                        등록번호
                    </th>
                    <th height="30px">
                        등록날짜
                    </th>
                    <th height="30px">
                        타이틀
                    </th>
                    <th height="30px">
                        등록갯수
                    </th>
                </thead>
                <tbody id="table-body">

                </tbody>
            </table>
        </div>
        <div class="panel-footer text-center" id="table-index">
            <ui class = "pagination">

            </ui>
        </div>
    </div>
</div>

<div class="modal fade" id="pw-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">비밀번호를 입력해 주세요</h4>
            </div>
            <div class="modal-body">
                Password: <input type="password" id="pw-area" placeholder="Password" style="ime-mode:inactive;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="submit">확인</button>
                <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>