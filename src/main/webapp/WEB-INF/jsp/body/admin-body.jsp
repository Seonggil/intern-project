<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class = "container" id = "body-container">
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">체크 리스트</h4>
            </div>
            <h3 class="col-md-8">항목을 선택해 주세요</h3>
            <div class="modal-body" id="criterion-items-box">

            </div>
            <div class="modal-body" id="selected-items-box">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="next-btn" data-mode="init">다음</button>
            </div>
        </div>
    </div>
</div>