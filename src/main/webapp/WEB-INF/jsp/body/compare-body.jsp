<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="container-fluid " style="padding-top: 50px; padding-left: 0px">
    <div class="row">
        <div class="col-xs-3">
            <ul class="nav nav-sidebar">
                <li class="corp-tap " id="over"><a><span class="span-tap" style="font-size:1.5em; color:#92B3B7">OverView</span></a>
                </li>
            </ul>

            <ul class="nav nav-sidebar">
                <li class="corp-tap" id="integration"><a><span class="span-tap" style="font-size:1.5em; color:black">통합비교</span></a>
                </li>
            </ul>

            <ul class="nav nav-sidebar">
                <li class="corp-tap" id="tmon"><a><span class="span-tap"
                                                        style="font-size:1.5em; color:black">티몬 물품</span></a></li>
                <ul class="nav nav-sidebar" id="tmonList" style="display:none">

                </ul>
            </ul>

            <ul class="nav nav-sidebar">
                <li class="corp-tap" id="coupang"><a><span class="span-tap"
                                                           style="font-size:1.5em; color:black">쿠팡 물품</span></a></li>
                <ul class="nav nav-sidebar" id="coupangList" style="display:none">
                </ul>
            </ul>

            <ul class="nav nav-sidebar">
                <li class="corp-tap" id="wemake"><a><span class="span-tap"
                                                          style="font-size:1.5em; color:black">위메프 물품</span></a></li>
                <ul class="nav nav-sidebar" id="wemakeList" style="display:none">
                </ul>
            </ul>
        </div>

        <div class="col-xs-8" id="display-pannel">
            <h1 class="page-header">OverView</h1>
            <h2 class="sub-header" id="update-date"></h2>
            <div class="table-responsive" id="basic-compare-price" style="width: 900px; height: 500px;">
            </div>
            <h2 class="sub-header">배송비교</h2>
            <div class="table-responsive" id="basic-compare-delivery" style="width: 900px; height: 500px;">
            </div>
            <h2 class="sub-header">별점비교</h2>
            <div class="table-responsive" id="basic-compare-star" style="width: 900px; height: 500px;">
            </div>
        </div>
    </div>
</div>

<svg xmlns="http://www.w3.org/2000/svg" width="200" height="200" viewBox="0 0 200 200" preserveAspectRatio="none"
     style="visibility: hidden; position: absolute; top: -100%; left: -100%;">
    <defs></defs>
    <text x="0" y="10"
          style="font-weight:bold;font-size:10pt;font-family:Arial, Helvetica, Open Sans, sans-serif;dominant-baseline:middle">
        200x200
    </text>
</svg>
