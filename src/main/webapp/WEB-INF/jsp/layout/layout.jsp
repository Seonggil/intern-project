<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><tiles:insertAttribute name="title" ignore="true" /></title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <style type="text/css" >
            .wrap-loading{ /*화면 전체를 어둡게 합니다.*/
                position: fixed;
                left:0;
                right:0;
                top:0;
                bottom:0;
                background: rgba(0,0,0,0.2); /*not in ie */
                filter: progid:DXImageTransform.Microsoft.Gradient(startColorstr='#20000000', endColorstr='#20000000');    /* ie */
            }
            .wrap-loading div{ /*로딩 이미지*/
                background: rgba(0,0,0,0.2);
                position: fixed;
                top:50%;
                left:50%;
                margin-left: -21px;
                margin-top: -21px;
            }
            .display-none{ /*감추기*/
                display:none;
            }
        </style>

        <tiles:insertAttribute name="header" ignore="true" />
        <tiles:insertAttribute name="body" ignore="true" />

    </body>
</html>
