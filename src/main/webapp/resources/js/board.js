const serverHostName = 'http://52.79.103.237:8081/seonggil/api';
const hostName = 'http://52.79.103.237:8081/seonggil/';
const indexSize = 5;

$(document).ready(function () {
    var boardIndex = 1;
    var bubbleFlag = false;


    $.attachBoardView = function (jsonData) {
        console.log(jsonData);

        $regitAmount = jsonData.registAmount == null ? "" : jsonData.registAmount;
        $idx = jsonData.index == null ? "" : jsonData.index;
        console.log("index:" + $idx);

        $startIdx = parseInt(($idx / 5));
        $endIdx = parseInt($regitAmount / 11);


        jsonData = jsonData.boardInfo;
        $.each(jsonData, function ($index, item) {

            $registNo = item.registNo == null ? "" : item.registNo;
            $registDT = item.registDT == null ? "" : item.registDT;
            $title = item.title == null ? "" : item.title;
            $itemAmount = item.itemAmount == null ? "" : item.itemAmount;

            var timestamp = $registDT;
            var date = new Date(timestamp);
            date.setHours(date.getHours()-9);

            $('#table-body').append(
                '<tr class = "regist-data" id = "data' + $registNo + '" data-regit-no= ' + $registNo + ' style = "cursor:pointer;">' +
                '<a href="">' +
                '   <td class="delete-img" data-regit-no=' + $registNo + '>' +
                '       <img src="/seonggil/resources/image/remove-sign.png">' +
                '   </td>' +
                '   <td>' +
                $registNo+
                '   </td>' +
                '   <td>' +
                date.toLocaleString()+
                '   </td>' +
                '   <td>' +
                $title +
                '   </td>' +
                '   <td>' +
                $itemAmount +
                '   </td>' +
                '</a>' +
                '</tr>'
            );

        });

        $('.pagination').append(
            '<li class="index-list" id = "prev"><a><<</a></li>'
        );

        for (var i = $startIdx; i < indexSize; i++) {
            $('.pagination').append(
                '<li class="index-list" id =' + i + '><a>' + (i + 1) + '</a></li>'
            );
            if (i == $endIdx) {
                break;
            }
        }

        $('.pagination').append(
            '<li class="index-list" id ="next"><a>>></a></li>'
        );

        $(".index-list").removeClass('active');
        $('#' + (boardIndex - 1)).addClass('active')
    };

    var getBoardData = function () {
        if (isNaN(boardIndex)) {
            boardIndex = 1;
        }
        $.ajax({
            url: encodeURI(serverHostName + '/board/main?index=' + boardIndex), // 요청 할 주소
            async: true, // false 일 경우 동기 요청으로 변경
            type: 'GET',
            success: function (jsonData) {
                $.attachBoardView(jsonData);
            },
            error: function () {
                alert('Error:죄송합니다.');
            }
        });
    };

    getBoardData();


    $.getListAmount = function () {
        var listAmount;
        $.ajax({
            url: encodeURI(serverHostName + '/board/list-amount'), // 요청 할 주소
            async: true, // false 일 경우 동기 요청으로 변경
            type: 'GET',
            success: function (jsonData) {
                listAmount = jsonData.amount;
            },
            error: function () {
                alert('Error:죄송합니다.');
            }
        });

        return listAmount;
    };

    $(document).on("click", "li", function () {
        var tmp;
        console.log("클릭");

        if ($(this).attr("id") == "prev") {

            if (parseInt((boardIndex) / 6) > 0) {
                tmp = boardIndex - (boardIndex % 6);
            } else {
                return;
            }

        } else if ($(this).attr("id") == "next" && boardIndex) {

            var listAmount = $.getListAmount();
            var maxGroupNo = parseInt(listAmount / 6);

            if (listAmount % 6 > 0) {
                maxGroupNo++;
            }

            if (maxGroupNo > parseInt((boardIndex) / 6)) {
                tmp = boardIndex + (6 - boardIndex);
            } else {
                return;
            }

        } else {
            tmp = parseInt($(this).text());
        }

        boardIndex = tmp;
        $(".regist-data").remove();
        $(".index-list").remove();
        getBoardData();
    });


    $(document).on("click", ".regist-data", function () {
        if (bubbleFlag) {
            bubbleFlag = false;
            return;
        }
        var regitNo = $(this).attr("data-regit-no");
        console.log(regitNo + " 번째 테이블 클릭");
        var addr = hostName + 'compare-view?regist-no=' + regitNo;
        console.log("addr:" + addr);
        window.location = addr
    });

    var deleteTr;

    $(document).on("click", ".delete-img", function () {
        bubbleFlag = true;
        deleteTr = $(this).attr("data-regit-no");
        $('#pw-modal').modal();
    });

    $(document).on("click", "#submit", function () {
        var errorFlag = false;

        $(".bg-danger").remove();
        var pw = $("#pw-area").val();
        $.ajax({
            url: encodeURI(serverHostName + '/board/checking?regist-no=' + deleteTr + '&pw=' + pw), // 요청 할 주소
            async: false, // false 일 경우 동기 요청으로 변경
            type: 'POST',
            success: function (jsonData) {
                if (!jsonData.loginValidation) {
                    $(".modal-body").append(
                        '<p class="bg-danger">비밀번호 오류</p>'
                    )
                    errorFlag = true;
                } else {
                    $(".bg-danger").remove();
                }
            },
            error: function () {
                $(".bg-danger").remove();
                $(".modal-body").append(
                    '<p class="bg-danger">오류</p>'
                )
                errorFlag = true;
            }
        });

        if (errorFlag) {
            return;
        }

        $.ajax({
            url: encodeURI(serverHostName + '/board/main?regist-no=' + deleteTr), // 요청 할 주소
            async: false, // false 일 경우 동기 요청으로 변경
            type: 'DELETE',
            success: function () {
                alert('삭제되었습니다.');
                $('#submit').attr('disabled',true);
                $('#data' + $registNo).remove();
            },
            error: function () {
                alert('삭제오류');
                $(".bg-danger").remove();
                $(".modal-body").append(
                    '<p class="bg-danger">오류</p>'
                )
            }
        });
    });

    $('#pw-modal').on('hidden.bs.modal', function (e) {
        console.log("모달 닫힘");
        $('#submit').attr('disabled',false);
        $('#pw-area').val("");
        $('.bg-danger').remove();
        $('#submit').removeClass('display-none');
    })

});