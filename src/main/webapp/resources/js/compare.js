const serverHostName = 'http://52.79.103.237:8081/seonggil/api';
const TMON_COLOR = 'rgb(255, 80, 0)';
const COUPANG_COLOR = 'rgb(81, 17, 16)';
const WEMAKE_COLOR = 'rgb(215, 54, 69)';

$(document).ready(function () {

    var tmonDealList = new Array;
    var coupangDealList = new Array;
    var wemakeDealList = new Array;

    $.makeTimestamp = function (date, timeType) {
        var dateObject;
        if (timeType == 'start') {
            dateObject = new Date(date + ' 00:00:00');
        } else if (timeType == 'end') {
            dateObject = new Date(date + ' 23:59:59');
        }

        return dateObject.getTime();
    };

    /**
     * @param Date 객체
     * @returns {출력 될 시간을 만든 String}
     * Date 객체에 -9 시간을 하여 한국 시간을 바꾼 String을 돌려준다
     */

    $.makeDateString = function (date) {
        date.setHours(date.getHours()-9);
        var str = date.toLocaleString();
        console.log("test" + str);

        if (str.charAt(str.length - 1) == '시') {
            return str.split(":")[0];
        } else {
            return str.split(":")[0] + '시';
        }
    };

    $.drawBasicPriceInfo = function (jsonData) {
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Deal Name');
            data.addColumn('number', 'Price');
            data.addColumn({type: 'string', role: 'style'});

            stdPrice = jsonData.standardDealInfo.price;

            data.addRows([
                ['<기준> ' + jsonData.standardDealInfo.name, stdPrice, TMON_COLOR]
            ]);

            var sltJson = jsonData.selectedDealInfo;
            $.each(sltJson, function ($index, item) {
                var color = '';
                if (item.corpName == '티몬') {
                    color = TMON_COLOR;
                } else if (item.corpName == '쿠팡') {
                    color = COUPANG_COLOR;
                } else if (item.corpName == '위메프') {
                    color = WEMAKE_COLOR;
                }

                data.addRows([
                    [item.name + '(' + item.corpName + ')', stdPrice + item.price, color]
                ]);
            });

            var options = {
                width: 900,
                legend: {},
                bar: {groupWidth: "20%"},
                axes: {
                    x: {
                        0: {side: 'Deal Name', label: 'Price'} // Top x-axis.
                    }
                },
                chartArea: {width: '50%'},
                vAxis: {gridlines: {count: 4}}
            };
            var chart = new google.visualization.ColumnChart(document.getElementById('basic-compare-price'));
            chart.draw(data, options);
        }
    };

    $.drawBasicStarInfo = function (jsonData) {
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Deal Name');
            data.addColumn('number', 'Star Score');
            data.addColumn({type: 'string', role: 'style'});

            stdStarScore = jsonData.standardDealInfo.itemReviewScore;
            data.addRows([
                ['<기준> ' + jsonData.standardDealInfo.name, stdStarScore, TMON_COLOR]
            ]);

            var sltJson = jsonData.selectedDealInfo;

            $.each(sltJson, function ($index, item) {
                var color = '';
                if (item.corpName == '티몬') {
                    color = TMON_COLOR;
                } else if (item.corpName == '쿠팡') {
                    color = COUPANG_COLOR;
                } else if (item.corpName == '위메프') {
                    color = WEMAKE_COLOR;
                }

                data.addRows([
                    [item.name + '(' + item.corpName + ')', stdStarScore + item.itemReviewScore, color]
                ]);
            });

            var options = {
                width: 900,
                bar: {groupWidth: "20%"},
                axes: {
                    x: {
                        0: {side: 'Deal Name', label: 'Star Score'} // Top x-axis.
                    }
                },
                chartArea: {width: '50%'},
                vAxis: {viewWindow: {min: 0, max: 5}, viewWindowMode: "explicit"}
            };
            var chart = new google.visualization.ColumnChart(document.getElementById('basic-compare-star'));
            chart.draw(data, options);
        }

    };

    $.drawBasicdeliInfo = function (jsonData) {
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawStuff);


        function drawStuff() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Deal Name');
            data.addColumn('number', 'Delivery Fee');
            data.addColumn({type: 'string', role: 'style'});

            deliveryFee = jsonData.standardDealInfo.deliveryFee;

            data.addRows([
                ['<기준> ' + jsonData.standardDealInfo.name, deliveryFee, TMON_COLOR]
            ]);


            var sltJson = jsonData.selectedDealInfo;

            $.each(sltJson, function ($index, item) {
                var color = '';
                if (item.corpName == '티몬') {
                    color = TMON_COLOR;
                } else if (item.corpName == '쿠팡') {
                    color = COUPANG_COLOR;
                } else if (item.corpName == '위메프') {
                    color = WEMAKE_COLOR;
                }

                data.addRows([
                    [item.name + '(' + item.corpName + ')', deliveryFee + item.deliveryFee, color]
                ]);
            });


            var options = {
                width: 900,
                bar: {groupWidth: "20%"},
                axes: {
                    x: {
                        0: {side: 'Deal Name', label: 'Delivery Fee'} // Top x-axis.
                    }
                },
                chartArea: {width: '50%'},
                vAxis: {viewWindow: {min: 0}, viewWindowMode: "explicit"}
            };
            var chart = new google.visualization.ColumnChart(document.getElementById('basic-compare-delivery'));
            chart.draw(data, options);
        }
    };


    $.categorizeDeal = function (jsonData) {
        tmonDealList.push(jsonData.standardDealInfo);

        $('#tmonList').append(
            '<li class="item" data-corp="tmon" id="' + jsonData.standardDealInfo.dealSrl + '" style="padding-left: 3.0em">' +
            '       <span  style="color:darkred;">' +
            '<기준>' + jsonData.standardDealInfo.name +
            '       </span>' +
            '</li>'
        );

        var sltJson = jsonData.selectedDealInfo;

        $.each(sltJson, function ($index, item) {
            if (item.corpName == "티몬") {
                tmonDealList.push(item);
                $('#tmonList').append(
                    '<li class="item" data-corp="tmon" id="' + item.dealSrl + '" style="padding-left: 3.0em">' +
                    '       <span style="color:black">' +
                    item.name +
                    '       </span>' +
                    '</li>'
                );
            } else if (item.corpName == "쿠팡") {
                coupangDealList.push(item);
                $('#coupangList').append(
                    '<li class="item" data-corp="coupang" id="' + item.dealSrl + '" style="padding-left: 3.0em">' +
                    '       <span  style="color:black">' +
                    item.name +
                    '       </span>' +
                    '</li>'
                );
            } else if (item.corpName == "위메프") {
                wemakeDealList.push(item);
                $('#wemakeList').append(
                    '<li class="item" data-corp="wemake" id="' + item.dealSrl + '" style="padding-left: 3.0em">' +
                    '       <span  style="color:black">' +
                    item.name +
                    '       </span>' +
                    '</li>'
                );
            }
        });
    };


    /**
     * OverView 시작 부분
     */
    $.getOverViewInfo = function () {
        var split = location.href.split("?");
        $.ajax({
            url: encodeURI(serverHostName + '/compare/basic-info?' + split[1]), // 요청 할 주소
            async: true, // false 일 경우 동기 요청으로 변경
            type: 'GET',
            success: function (jsonData) {
                var timestamp = jsonData.standardDealInfo.updateDT;
                var dateStr = $.makeDateString(new Date(timestamp));
                dateStr = dateStr;
                $('#update-date').text('업데이트 날짜:' + dateStr);
                $.categorizeDeal(jsonData);
                $.drawBasicPriceInfo(jsonData);
                $.drawBasicdeliInfo(jsonData);
                $.drawBasicStarInfo(jsonData);
            },
            error: function () {
                alert('Error:죄송합니다.');
            }
        });

    };
    $.getOverViewInfo();


    $.attachDatePickView = function (mode) {
        $('#display-pannel').append(
            '<div class="row">' +
            '   <div class="col-xs-3">' +
            '       <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>' +
            '       <input type="text" id="start-datepicker" class="form-control docs-date" name="date" placeholder="시작일">' +
            '   </div>' +
            '   <div class="col-xs-3">' +
            '       <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>' +
            '       <input type="text" id="end-datepicker" class="form-control docs-date" name="date" placeholder="종료일">' +
            '   </div>' +
            '   <button type="button" id="search-btn" class="btn btn-info" data-button-mode ="default" style="margin-top:20px">조회</button>' +
            '</div>'
        );


        $('#search-btn').attr('data-button-mode', mode);

        $(function () {
            $('#start-datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy/mm/dd',
                prevText: '이전 달',
                nextText: '다음 달',
                closeText: 'CLOSE',
                maxDate: "+0D",
            });
        });
        $(function () {
            $('#end-datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                currentText: 'TODAY',
                dateFormat: 'yy/mm/dd',
                prevText: '이전 달',
                nextText: '다음 달',
                closeText: 'CLOSE',
                maxDate: "+0D",
            });
        });
    };

    $.initIntergrationDisp = function () {
        $('#display-pannel').empty();
        $('#display-pannel').append(
            '<h1 class="page-header">통합비교</h1>' +
            '<h2 class="sub-header">조회기간 선택</h2>'
        );
        $.attachDatePickView('integration');
        $('#display-pannel').append(
            '<div class="table-responsive" id="price-line" style="width: 900px; height: 500px;">' +
            '</div>' +
            '<div class="table-responsive" id="star-line" style="width: 900px; height: 500px;">' +
            '</div>' +
            '<div class="table-responsive" id="deli-fee-line" style="width: 900px; height: 500px;">' +
            '</div>' +
            '<div class="table-responsive" id="review-amount-line" style="width: 900px; height: 500px;">' +
            '</div>'
        )
    };


    /**
     * 사이드바 클릭 시 행동
     */
    $(document).on("click", ".corp-tap", function () {
        $('.span-tap').css('color', 'black');
        $(this).find('span').css('color', '#92B3B7');

        var corpName = $(this).attr('id');
        console.log('corpName : ' + corpName);
        var attachId;

        $('#display-pannel').empty();
        $('#display-pannel').append(
            '<h1 class="page-header"></h1>' +
            '<h2 class="sub-header">조회기간 선택</h2>'
        );

        if (corpName == 'over') {
            location.replace($(location).attr('href'));
        } else if (corpName == 'integration') {
            $("#tmonList").css("display", "none");
            $("#coupangList").css("display", "none");
            $("#wemakeList").css("display", "none");
            $.initIntergrationDisp();
        }
        else if (corpName == 'tmon') {
            $("#tmonList").css("display", "block");
            $("#coupangList").css("display", "none");
            $("#wemakeList").css("display", "none");
            $('#page-header').text("티몬 물품 간 비교");
            $.attachDatePickView(corpName);
        } else if (corpName == 'coupang') {
            $("#tmonList").css("display", "none");
            $("#coupangList").css("display", "block");
            $("#wemakeList").css("display", "none");
            $('#page-header').text("쿠팡 물품 간 비교");
            $.attachDatePickView(corpName);
        } else if (corpName == 'wemake') {
            $("#tmonList").css("display", "none");
            $("#coupangList").css("display", "none");
            $("#wemakeList").css("display", "block");
            $('#page-header').text("위메프 물품 간 비교");
            $.attachDatePickView(corpName);
        }
    });


    /**
     * @param listSet
     * @param dataType
     * @returns {Array}
     * description : 통합 검색시 타입에 따라 그래프에 데이터를 넣어주는 함수
     */
    $.makeLineChartData = function (listSet, dataType) {
        var timestamp = listSet[0].updateDT;
        var dateStr = $.makeDateString(new Date(timestamp));
        var dataArry = [];

        dataArry.push(dateStr);

        $.each(listSet, function ($index, item) {
            if (dataType == 'price') dataArry.push(item.price);
            else if (dataType == 'star') dataArry.push(item.itemReviewScore);
            else if (dataType == 'deliFee') dataArry.push(item.deliveryFee);
            else if (dataType == 'reviewAmount') dataArry.push(item.reviewAmount);
            else if (dataType == 'saleAmount') dataArry.push(item.saleAmount);
            else if (dataType == 'deliveryReview') dataArry.push(item.deliveryReviewScore);
        });

        return dataArry;
    };

    /**
     * 라인 그래프 초기화
     */

    $.initLineGraph = function (jsonData) {
        var setAmount = jsonData.list0.length;
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Update Date');
        for (var i = 0; i < setAmount; i++) {
            data.addColumn('number', jsonData.list0[i].name);
        }

        return data;
    }

    /**
     * 가격 라인그래프
     */
    $.drawPriceComp = function (jsonData) {
        google.charts.load('current', 1, {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawPriceChart);
        google.charts.setOnLoadCallback(drawStarChart);
        google.charts.setOnLoadCallback(drawDeliFeeChart);
        google.charts.setOnLoadCallback(drawReviewAmountChart);

        function drawPriceChart() {
            var data = $.initLineGraph(jsonData);

            $.each(jsonData, function ($index, item) {
                var pushData = $.makeLineChartData(item, 'price');
                data.addRow(pushData);
            });

            var options = {
                hAxis: {title: "Date"},
                vAxis: {title: 'Price', viewWindow: {min: 0}, viewWindowMode: "explicit"}
            };

            var chart = new google.visualization.LineChart(document.getElementById('price-line'));
            chart.draw(data, options);
        }

        function drawStarChart() {
            var data = $.initLineGraph(jsonData);

            $.each(jsonData, function ($index, item) {
                var pushData = $.makeLineChartData(item, 'star');
                data.addRow(pushData);
            });

            var options = {
                hAxis: {title: "Date"},
                vAxis: {title: 'Star', viewWindow: {min: 0, max: 5}, viewWindowMode: "explicit"}
            };

            var chart = new google.visualization.LineChart(document.getElementById('star-line'));
            chart.draw(data, options);
        }

        function drawDeliFeeChart() {
            var data = $.initLineGraph(jsonData);

            $.each(jsonData, function ($index, item) {
                var pushData = $.makeLineChartData(item, 'deliFee');
                data.addRow(pushData);
            });

            var options = {
                hAxis: {title: "Date"},
                vAxis: {title: 'Delivery Fee', viewWindow: {min: 0}, viewWindowMode: "explicit"}
            };

            var chart = new google.visualization.LineChart(document.getElementById('deli-fee-line'));
            chart.draw(data, options);
        }

        function drawReviewAmountChart() {
            var data = $.initLineGraph(jsonData);

            $.each(jsonData, function ($index, item) {
                var pushData = $.makeLineChartData(item, 'reviewAmount');
                data.addRow(pushData);
            });

            var options = {
                hAxis: {title: "Date"},
                vAxis: {title: 'Review Amount', viewWindow: {min: 0}, viewWindowMode: "explicit"}
            };

            var chart = new google.visualization.LineChart(document.getElementById('review-amount-line'));
            chart.draw(data, options);
        }
    };

    /**
     * 조회 시작
     */

    $.drawSaleAmountGraph = function (jsonData, elementID) {
        google.charts.load('current', 1, {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawPriceChart);


        function drawPriceChart() {
            var data = $.initLineGraph(jsonData);

            $.each(jsonData, function ($index, item) {
                if (elementID == 'sale-amount') {
                    var pushData = $.makeLineChartData(item, 'saleAmount');
                } else if (elementID == 'delivery-review') {
                    var pushData = $.makeLineChartData(item, 'deliveryReview');
                }
                data.addRow(pushData);
            });

            var options = {
                hAxis: {title: "Date"},
                vAxis: {title: 'Sale Amount', viewWindow: {min: 0}, viewWindowMode: "explicit"}
            };

            var chart = new google.visualization.LineChart(document.getElementById(elementID));
            chart.draw(data, options);
        }
    };

    $.compareTmonDeal = function (jsonData) {
        $('#display-pannel').append(
            '<h2 class="sub-header" style="margin-top: 50px">판매량 비교</h2>'+
            '<div class="table-responsive" id="sale-amount" style="width: 900px; height: 500px;">' +
            '</div>' +
            '<h2 class="sub-header"style="margin-top: 50px">배송 리뷰 별점 비교</h2>'+
            '<div class="table-responsive" id="delivery-review" style="width: 900px; height: 500px;">' +
            '</div>'+
            '<h2 class="sub-header" style="margin-top: 100px">배송 타입 비교</h2>'+
            '<div class="table-responsive" id="delivery-type" style="width: 900px; height: 500px;">' +
            '</div>'
        );
        $.drawSaleAmountGraph(jsonData, 'sale-amount');
        $.drawSaleAmountGraph(jsonData, 'delivery-review');
        $('#delivery-type').append(
            '<table class="table table-bordered table-list">' +
            '   <thead>' +
            '       <th height="30px" >상품 이름</th>' +
            '       <th height="30px" >무료배송</th>' +
            '       <th height="30px"> 무료반품 </th>' +
            '       <th height="30px"> 조건 무료 배송 </th>' +
            '       <th height="30px"> 날짜지정배송 </th>' +
            '       <th height="30px"> 당일배송 </th>' +
            '       <th height="30px"> 슈퍼마트배송 </th>' +
            '       <th height="30px"> 책임배송 </th>' +
            '   </thead>' +
            '<tbody id="table-body"> </tbody>' +
            '</table>'
        );

        var lastData = Object.keys(jsonData).length;
        console.log("lastData:" + jsonData['list'+(lastData-1)]);

        $.each(jsonData['list'+(lastData-1)], function ($index, item) {
            console.log("foreach");
            $('#table-body').append(
                '<tr>' +
                '   <td id="deal-name-td' + $index + '"></td>' +
                '   <td id="deal-freeDelivery-td' + $index + '"></td>' +
                '   <td id="deal-freeReturn-td' + $index + '"></td>' +
                '   <td id="deal-conditionalFreeDeli-td' + $index + '"></td>' +
                '   <td id="deal-dayChoiceDelivery-td' + $index + '"></td>' +
                '   <td id="deal-sendToday-td' + $index + '"></td>' +
                '   <td id="deal-superMartDelivery-td' + $index + '"></td>' +
                '   <td id="deal-responseDelivery-td' + $index + '"></td>' +
                '</tr>'
            );

            $('#deal-name-td' + $index).append(
                '<p>' + item.name + '</p>'
            )

            if (item.freeDelivery) {
                $('#deal-freeDelivery-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.freeReturn) {
                $('#deal-freeReturn-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.dayChoiceDelivery) {
                $('#deal-dayChoiceDelivery-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.sendToday) {
                $('#deal-sendToday-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.superMartDelivery) {
                $('#deal-superMartDelivery-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.responseDelivery) {
                $('#deal-responseDelivery-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if(item.conditionalFreeDeli) {
                $('#deal-conditionalFreeDeli-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
        });
    };

    $.compareCoupangDeal = function (jsonData) {
        $('#display-pannel').append(
            '<h2 class="sub-header" style="margin-top: 100px">배송 타입 비교</h2>'+
            '<div class="table-responsive" id="delivery-type" style="width: 900px; height: 500px;">' +
            '</div>'
        );

        $('#delivery-type').append(
            '<table class="table table-bordered table-list">' +
            '   <thead>' +
            '       <th height="30px" >상품 이름</th>' +
            '       <th height="30px" >무료배송</th>' +
            '       <th height="30px"> 무료반품 </th>' +
            '       <th height="30px"> 조건 무료 배송 </th>' +
            '       <th height="30px"> 로켓배송 </th>' +
            '       <th height="30px"> 정기배송 </th>' +
            '   </thead>' +
            '<tbody id="table-body"> </tbody>' +
            '</table>'
        );

        var lastData = Object.keys(jsonData).length;
        console.log("lastData:" + jsonData['list'+(lastData-1)]);

        $.each(jsonData['list'+(lastData-1)], function ($index, item) {
            console.log("foreach");
            $('#table-body').append(
                '<tr>' +
                '   <td id="deal-name-td' + $index + '"></td>' +
                '   <td id="deal-freeDelivery-td' + $index + '"></td>' +
                '   <td id="deal-freeReturn-td' + $index + '"></td>' +
                '   <td id="deal-conditionalFreeDeli-td' + $index + '"></td>' +
                '   <td id="deal-rocketDelivery-td' + $index + '"></td>' +
                '   <td id="deal-intervalDelivery-td' + $index + '"></td>' +
                '</tr>'
            );

            $('#deal-name-td' + $index).append(
                '<p>' + item.name + '</p>'
            )

            if (item.freeDelivery) {
                $('#deal-freeDelivery-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.freeReturn) {
                $('#deal-freeReturn-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.rocketDelivery) {
                $('#deal-rocketDelivery-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.intervalDelivery) {
                $('#deal-intervalDelivery-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if(item.conditionalFreeDeli) {
                $('#deal-conditionalFreeDeli-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
        });

    };

    $.compareWeMakeDeal = function (jsonData) {
        $('#display-pannel').append(
            '<h2 class="sub-header" style="margin-top: 50px">판매량 비교</h2>'+
            '<div class="table-responsive" id="sale-amount" style="width: 900px; height: 500px;">'+
            '</div>'+
            '<h2 class="sub-header" style="margin-top: 100px">배송 타입 비교</h2>'+
            '<div class="table-responsive" id="delivery-type" style="width: 900px; height: 500px;">' +
            '</div>'
        );


        $('#delivery-type').append(
            '<table class="table table-bordered table-list">' +
            '   <thead>' +
            '       <th height="30px" >상품 이름</th>' +
            '       <th height="30px" >무료배송 </th>' +
            '       <th height="30px"> 무료반품 </th>' +
            '       <th height="30px"> 조건무료배송 </th>' +
            '   </thead>' +
            '<tbody id="table-body"> </tbody>' +
            '</table>'
        );

        $.drawSaleAmountGraph(jsonData, 'sale-amount');
        var lastData = Object.keys(jsonData).length;
        console.log("lastData:" + jsonData['list'+(lastData-1)]);

        $.each(jsonData['list'+(lastData-1)], function ($index, item) {
            console.log("foreach");
            $('#table-body').append(
                '<tr>' +
                '   <td id="deal-name-td' + $index + '"></td>' +
                '   <td id="deal-freeDelivery-td' + $index + '"></td>' +
                '   <td id="deal-freeReturn-td' + $index + '"></td>' +
                '   <td id="deal-conditionalFreeDeli-td' + $index + '"></td>' +
                '</tr>'
            );

            $('#deal-name-td' + $index).append(
                '<p>' + item.name + '</p>'
            )

            if (item.freeDelivery) {
                $('#deal-freeDelivery-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.freeReturn) {
                $('#deal-freeReturn-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
            if (item.rocketDelivery) {
                $('#deal-conditionalFreeDeli-td' + $index).append(
                    '<img src="/seonggil/resources/image/check.png">'
                )
            }
        });

    };

    $(document).on("click", "#search-btn", function () {
        var startTimestamp = $.makeTimestamp($('#start-datepicker').val(), 'start');
        var endTimestamp = $.makeTimestamp($('#end-datepicker').val(), 'end');
        var mode = $(this).attr('data-button-mode');
        var url = '';


        if (endTimestamp < startTimestamp) {
            alert("날짜 입력 오류");
            return;
        }

        if (mode == 'integration') {
            url = serverHostName + '/compare/integration?startDate=' + startTimestamp + '&endDate=' + endTimestamp + '&regitNo='
                + document.location.href.split("?")[1].split('=')[1];
        } else {
            url = serverHostName + '/compare/corpIntegration?startDate=' + startTimestamp + '&endDate=' + endTimestamp + '&regitNo='
                + document.location.href.split("?")[1].split('=')[1] + '&corpName=' + mode;
        }
        console.log('url : ' + url);
        console.log('#search-btn mode : ' + mode);

        $.ajax({
            url: encodeURI(url),
            async: true, // false 일 경우 동기 요청으로 변경
            type: 'GET',
            success: function (jsonData) {
                if (jsonData.hasOwnProperty('list0')) {
                    if (mode == 'integration') {
                        $.drawPriceComp(jsonData);
                    } else if (mode == 'tmon') {
                        $('.page-header').text('티몬 물품간 비교');
                        $.compareTmonDeal(jsonData);
                    } else if (mode == 'coupang') {
                        $('.page-header').text('쿠팡 물품간 비교');
                        $.compareCoupangDeal(jsonData);
                    } else if (mode == 'wemake') {
                        $('.page-header').text('위메프 물품간 비교');
                        $.compareWeMakeDeal(jsonData);
                    }
                } else {
                    alert("검색 결과가 없습니다");
                }
            },
            error: function () {
                alert('Error:죄송합니다.');
            }
        });
    });

});