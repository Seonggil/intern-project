const serverHostName = 'http://52.79.103.237:8081/seonggil/api';
const TITLE_LENGTH = 30;
const PW_LENGTH = 15;
var searchContent;
var corpName = "티몬";
var items;
var selectItems = [];


$(document).ready(function () {

    $.parsingSearchData = function (jsonData) {
        var $dealName, $dealUrl, $amount, $imageUrl, $dealPrice, $dealNum;
        var $numberingRow;
        $.each(jsonData, function ($index, item) {

            if ($index % 3 == 0) {
                $numberingRow = 'row' + ($index / 3);
                $('#body-container').append("<div class='row item-row' id=" + $numberingRow + "></div>");
            }

            $dealName = item.name == null ? "" : item.name;
            $dealUrl = item.url == null ? "" : item.url;
            $imageUrl = item.imageUrl == null ? "" : item.imageUrl;
            $dealPrice = item.price == null ? "" : item.price;
            $dealNum = item.dealSrl == null ? "" : item.dealSrl;

            var itemObject = {
                name: $dealName,
                url: $dealUrl,
                imageUrl: $imageUrl,
                price: $dealPrice,
                dealSrl: $dealNum,
                corpName: corpName,
            };


            items.push(itemObject);

            $('#' + $numberingRow).append(
                '<div class = "col-sm-6 col-md-4 items" id = ' + $index + ' data-click-flag = 0 style="height:500px;">' +
                '   <div class="thumbnail" style="height:450px;">' +
                '       <img class="image-thumbnail img-responsive center-block" src='+ $imageUrl +'>' +
                '       <p  class="caption text-center lead">' + $dealName + '</p>' +
                '   </div>' +
                '</div>'
            );

            if (corpName == "쿠팡") {
                itemObject.rocketPrice = item.rocketPrice;
                itemObject.intervalPrice = item.intervalPrice;
                if (itemObject.rocketPrice > 0) {
                    $('#' + $index + ' .thumbnail').append(
                        '<p class="text-center leads" id="' + $index + '-1">  로켓배송가격 = ' + itemObject.rocketPrice + '원</p>'
                    );
                } else {
                    $('#' + $index + ' .thumbnail').append('<p class="text-center leads" id="' + $index + '-3"> ' + $dealPrice + '원</p>');
                }
            } else {
                $('#' + $index + ' .thumbnail').append("<p class='text-center leads'>" + $dealPrice + "원</p>");
            }

        });
    };

    $.changeAttach = function (jsonData) {

        console.log("change Attach");
        var presentSearchType = $('#presentSearchType').text();
        items = [];

        $(".item-row").remove();

        if (jsonData.searchValidation == "false") {
            alert("검색결과가 없습니다.");
            return false;
        }

        $('.companyType').css('border-bottom', 'solid 0px')
        $('#' + corpName).css('border-bottom', 'solid 1px red');

        var jsonData = jsonData.dealList;

        console.log("corpName " + corpName);
        $.parsingSearchData(jsonData);
    };

    $.chageView = function (corpName, sorter) {
        var requestAddr;
        corpName = corpName.trim();
        console.log("name:" + corpName);
        if (corpName == "티몬") {
            requestAddr = serverHostName + "/search/list/tmon/" + searchContent + "?sort-type=";
        }
        else if (corpName == "쿠팡") {
            requestAddr = serverHostName + "/search/list/coupang/" + searchContent + "?sort-type=";
        }
        else if (corpName == "위메프") {
            requestAddr = serverHostName + "/search/list/we-make-price/" + searchContent + "?sort-type=";
        }

        requestAddr = requestAddr + sorter;
        requestAddr = encodeURI(requestAddr);
        console.log('encoding:'+requestAddr);

        $.ajax({
            url: requestAddr, // 요청 할 주소
            async: true, // false 일 경우 동기 요청으로 변경
            type: 'GET',
            dataType: 'json',
            success: function (jsonData) {
                $.changeAttach(jsonData)
            },
            error: function () {
                alert('데이터를 가져오는데 실패 하였습니다.');
            }
        });
    };

    $.initPageAttach = function (jsonData) {
        var presentSearchType = $('#presentSearchType').text();

        $('.masthead').remove();
        $(".row").remove();
        items = [];

        if (jsonData.searchValidation == "false") {
            alert("검색결과가 없습니다.");
            return false;
        }

        if (presentSearchType == '티몬 내 검색') {
            $('#body-container').append(
                '<div class="masthead" style="margin-bottom: 30px"> ' +
                '   <div class = row>' +
                '       <div class="col-md-8">' +
                '           <h3 class="text-muted">검색결과</h3> ' +
                '       </div>' +
                '       <div class="col-md-3 col-md-offset-1"  style="padding-top: 15px">' +
                '           <button type="button" class="btn btn-default" id="determine-save" >선택저장</button>' +
                '           <button type="button" class="btn btn-default" id="determine-finish" data-toggle="modal" data-target="#myModal" >선택완료</button>' +
                '       </div>' +
                '   </div>' +
                '</div>'
            );
        }
        else {
            $('#body-container').append(
                '<div class="masthead"  style="margin-bottom: 30px"> ' +
                '    <div class = row>' +
                '           <div class="col-md-8">' +
                '               <h3 class="text-muted">검색결과</h3> ' +
                '           </div>' +
                '           <div class="col-md-3 col-md-offset-1"  style="padding-top: 15px">' +
                '               <button type="button" class="btn btn-default" id="determine-save">선택저장</button>' +
                '               <button type="button" class="btn btn-default" id="determine-finish" data-toggle="modal" data-target="#myModal" >선택완료</button>' +
                '           </div>' +
                '   </div>' +
                '   <nav style="margin-top: 15px"> ' +
                '           <ul class="nav nav-justified"> ' +
                '               <li class="companyType" id="티몬"><a>티몬</a></li>' +
                '               <li class="companyType" id="쿠팡"><a>쿠팡</a></li> ' +
                '               <li class="companyType" id="위메프"><a>위메프</a></li> ' +
                '          </ul>' +
                '   </nav> ' +
                '   <p style="margin-top: 20px">' +
                '       <button type="button" class="btn btn-xs btn-default selectSortType">인기순</button>' +
                '       <button type="button" class="btn btn-xs btn-default selectSortType">최신순</button>' +
                '       <button type="button" class="btn btn-xs btn-default selectSortType">저가순</button>' +
                '       <button type="button" class="btn btn-xs btn-default selectSortType">고가순</button>' +
                '   </p>' +
                '</div>' +
                '</div>'
            );
        }

        jsonData = jsonData.dealList;

        $.parsingSearchData(jsonData);
        $('#티몬').css('border-bottom', 'solid 2px red');
    };

    var initSearchView = function () {
        searchContent = document.getElementById('search-contents').value;
        var requestAddr = serverHostName + '/search/list/tmon/' + searchContent;
        requestAddr = encodeURI(requestAddr);
        console.log('encoding:'+requestAddr);
        var param = "sort-type=open"

        $.ajax({
            url: requestAddr, // 요청 할 주소
            async: true, // false 일 경우 동기 요청으로 변경
            type: 'GET',
            data: param,
            success: function (jsonData) {
                $.initPageAttach(jsonData);
            },
            error: function () {
                alert('Error:죄송합니다.');
            }
        });
    };

    $('#nav-search-button').click(initSearchView);


    $('.searchType').click(function () {
        var name = $(this).text();
        $('#presentSearchType').text(name);
    });

    /**
     * 타 홈쇼핑 탭(검색결과 및 회사 선택 구역)을 눌렀을때 정보 가져오는 Jquery
     */
    $(document).on("click", ".companyType", function () {
        corpName = $(this).text();
        $.chageView(corpName, "favorite");
    });

    $(document).on("click", ".selectSortType", function () {
        var sortText = $(this).text();

        if (sortText == "인기순") {
            $.chageView(corpName, "favorite");
        } else if (sortText == "최신순") {
            $.chageView(corpName, "open");
        } else if (sortText == "저가순") {
            $.chageView(corpName, "cheap");
        } else if (sortText == "고가순") {
            $.chageView(corpName, "expensive");
        }
    });

    $(document).on("click", ".items", function () {
        var itemIndex = $(this).attr('id');
        if ($(this).attr('data-click-flag') == '0') {
            $('#' + itemIndex + ' .thumbnail').css('border-style', 'solid');
            $('#' + itemIndex + ' .thumbnail').css('border-color', '#ff0000');
            $('#' + itemIndex).attr('data-click-flag', '1');
        } else {
            $('#' + itemIndex + ' .thumbnail').css('border-color', '');
            $('#' + itemIndex).attr('data-click-flag', '0');
        }
    });

    /**
     * 아이템 저장 눌렀을때
     */
    $(document).on("click", "#determine-save", function () {
        var itemsLength = items.length;
        var selectItemAmount = 0;
        for (var i = 0; i < itemsLength; i++) {
            if ($('#' + i).attr('data-click-flag') == '1') {
                var overlapFlag = false;

                for (var j = 0; j < selectItems.length; j++) {
                    if (selectItems[j].dealSrl == items[i].dealSrl) {
                        overlapFlag = true;
                    }
                }

                if (!overlapFlag) {
                    selectItems.push(items[i]);
                }

                selectItemAmount++;
                $('#' + i + ' .thumbnail').css('border-color', '');
                $('#' + i).attr('data-click-flag', '0');
                selectItemAmount++;
            }
        }

        if (selectItemAmount == 0) {
            alert("항목을 선택하시지 않으셨습니다.");
            return;
        }
        console.log(selectItems.length);
    });

    /**
     * 이 세개의 변수는 정보 전달 후 초기화 요망
     */
    var sendItemData;
    var criteriaItem;
    var tmonItemfilerIndex;
    var indexSaver;

    /**
     * 선택 완료가 눌렸을때 기준을 찾기위한 항목 추가
     */

    $(document).on("click", "#determine-finish", function () {
        var reviseIndex = 0;
        tmonItemfilerIndex = [];

        console.log("선택완료");
        var selectItemsLength = selectItems.length;

        $('#criterion-items-box').append(
            '<div class =  "col-md-8">' +
            '   <h4 >기준 항목(티몬 물품)</h4>' +
            '</div>'
        );
        $('#criterion-items-box').append(
            '<div class = "col-md-8" id="item-list">' +
            '</div>'
        );

        for (var i = 0; i < selectItemsLength; i++) {
            if (selectItems[i].corpName == '티몬') {
                $('#item-list').append(
                    '<label>' +
                    '   <input class = "criterion-checklist" type="checkbox" id=' + reviseIndex + ' value="">' + selectItems[i].name +
                    '</label>' +
                    '</br>'
                )
                tmonItemfilerIndex.push(i);
                reviseIndex++;
            }
        }
    });


    /**
     * 중복기준을 정했는지 체크
     */
    $.checkingCriterionCheckbox = function () {
        var checkboxSize = $('#item-list > label').size();
        var duplicationCheckFlag = 0, itemIndex;


        for (var i = 0; i < checkboxSize; i++) {

            if (duplicationCheckFlag == 0 && $('#item-list  #' + i).prop("checked")) {
                criteriaItem = selectItems[tmonItemfilerIndex[i]];
                itemIndex = tmonItemfilerIndex[i];
                duplicationCheckFlag = 1;
                console.log("checkBox 확인");
            } else if (duplicationCheckFlag == 1 && $('#item-list  #' + i).prop("checked")) {
                $('#item-list').append('<p class="bg-danger">기준을 중복으로 잡을 수 없습니다.</p>');
                return false;
            }
        }

        if (duplicationCheckFlag == 0) {
            $('#item-list').append('<p class="bg-danger">하나 이상은 체크 하셔야 합니다</p>');
            return false;
        }

        return true;
    };

    /**
     * 비교당할 리스트 추가 함수
     */
    $.addSeletedItems = function () {
        indexSaver = [];
        console.log("선택당할 아이템 리스트");
        var selectItemsLength = selectItems.length;
        var attachIdNum = 0;

        $('#selected-items-box').append(
            '<div class =  "col-md-8">' +
            '   <h4 >비교할 상품</h4>' +
            '</div>'
        );
        $('#selected-items-box').append(
            '<div class = "col-md-8" id="item-list">' +
            '</div>'
        );

        for (var i = 0; i < selectItemsLength; i++) {

            if (selectItems[i].dealSrl != criteriaItem.dealSrl) {
                indexSaver.push(i);

                $('#item-list').append(
                    '<label>' +
                    '   <input class = "criterion-checklist" type="checkbox" id=' + attachIdNum + ' value="">(' + selectItems[i].corpName + ')' + selectItems[i].name +
                    '</label>' +
                    '</br>'
                )

                attachIdNum++;
            }
        }

        $('#item-list').append('<input type="text" id="title"class="form-control" placeholder="Title">' +
            '<input type="password" class="form-control" id="pw" placeholder="Password" style="ime-mode:inactive">'
        );
    };

    /**
     * 비교당할 리스트 체크를 보내기 위한 배열에 담기
     */
    $.selectCheckItems = function () {
        var selectAmount = 0;
        sendItemData = [];
        var checkboxSize = $('#item-list > label').size();
        console.log("checkBox Size:" + checkboxSize);

        for (var i = 0; i < checkboxSize; i++) {
            if ($('#item-list  #' + i).prop("checked")) {
                sendItemData.push(selectItems[indexSaver[i]]);
                selectAmount++;
                console.log("선택상품이름" + sendItemData[selectAmount - 1].name);
            }
        }

        console.log("title:" + $('#title').val());
        console.log("pw:" + $('#pw').val());

        if ($('#title').val() == "" || $('#title').val() == "Title") {
            $('#item-list').append('<p class="bg-danger">제목을 입력해 주세요</p>');
            title = "";
            return false;
        }

        if ($('#title').val().length > TITLE_LENGTH) {
            $('#item-list').append('<p class="bg-danger">제목을 30글자 아래로 입력해 주세요</p>');
            title = "";
            return false;
        }

        if ($('#pw').val() == "") {
            $('#item-list').append('<p class="bg-danger">비밀번호를 입력해 주세요</p>');
            pw = "";
        }

        if ($('#pw').val().length > PW_LENGTH) {
            $('#item-list').append('<p class="bg-danger">비밀번호를 15글자 아래로 입력해 주세요</p>');
            pw = "";
        }

        if (selectAmount > 0) {
            console.log("sendDataSize:" + sendItemData.length);
            return true;
        } else {
            $('#item-list').append('<p class="bg-danger">하나 이상은 체크 하셔야 합니다</p>');
            return false;
        }
    };


    $.sendSelectedData = function (titleValue, passwordValue) {

        var sendJsonDataObj = {
            title: titleValue,
            pw: passwordValue,
            baseItem: criteriaItem,
            selectedItems: sendItemData
        };

        sendJsonDataObj = JSON.stringify(sendJsonDataObj);

        console.log(sendJsonDataObj.toString());

        $('#next-btn').text('닫기');

        var url = serverHostName + "/register/deal-detail-info";
        console.log(url);
        console.log(sendJsonDataObj.toString());

        console.log("beforeSend")
        $('#criterion-items-box').append('<div class="wrap-loading display-none">' +
            ' <div style="background: #000000; background : rgba(0, 0, 0, 0.0);">' +
            '   <img src="/seonggil/resources/image/ajax_loader.gif" /></div>' +
            '</div>');
        $('.wrap-loading').removeClass('display-none');

        $('#next-btn').hide();

        $.ajax({
            url: encodeURI(url), // 요청 할 주소
            async: true, // false 일 경우 동기 요청으로 변경
            type: "json",
            method: "post",
            contentType: "application/json",
            data: sendJsonDataObj,

            success: function () {
                $('.wrap-loading').addClass('display-none');
                $('#selected-items-box').append('<h4 >등록 되었습니다</h4>');
                selectItems = [];
            },
            error: function () {
                $('.wrap-loading').addClass('display-none');
                $('#selected-items-box').append('<h4 >등록에 실패하였습니다</h4>');
            }
        });
    };

    $(document).on("click", "#next-btn", function () {
        if ($('#next-btn').attr('data-mode') == 'init') {//기준 선택 후 다음 버튼
            console.log("init 상태 test");
            if (!$.checkingCriterionCheckbox()) {
                return;
            }

            $('#criterion-items-box').empty();
            $('#selected-items-box').empty();

            $.addSeletedItems();
            $('#next-btn').attr('data-mode', 'finish');
        } else if ($('#next-btn').attr('data-mode') == 'finish') {//전송 단계에서 하는일

            $('#next-btn').text('전송');
            if (!$.selectCheckItems()) {
                return;
            }

            var titleValue, passwordValue;
            passwordValue = $('#pw').val()
            titleValue = $('#title').val();


            $('#next-btn').attr('data-mode', 'init');
            $('#criterion-items-box').empty();
            $('#selected-items-box').empty();

            $.sendSelectedData(titleValue, passwordValue);
        }
    });

    $(document).on("click", ".close", function () {
        $('#next-btn').show();
        console.log("닫힘 버튼 눌림");
        $('#next-btn').text('다음');
        $('#next-btn').attr('data-mode', 'init');
        $('#criterion-items-box').empty();
        $('#selected-items-box').empty();
    });


    $('#myModal').on('hidden.bs.modal', function (e) {
        $('#next-btn').show();
        console.log("모달 닫기");
        $('#next-btn').attr('data-mode', 'init');
        $('#criterion-items-box').empty();
        $('#selected-items-box').empty();
    })
});

